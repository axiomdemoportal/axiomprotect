/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mollatech.axiom.nucleus.crypto.AES;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.crypto.PropsFileUtil;
import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 *
 * @author Ideasventure
 */
public class ChangeDBPassword {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        try {

            String sep = System.getProperty("file.separator");
            String path = sep + "var" + sep + "axiomprotect2" + sep + "interface";

            //  System.out.println(oldDBPass);
            String usrhome = path;
            usrhome += sep + "axiomv2-settings";
            String g_strPath = usrhome + sep;
            String filepath = usrhome + sep + "dbsetting.conf";

            Console c = System.console();
            if (c == null) {
                System.err.println("No console.");
                System.exit(1);
            }

            char[] oldPassword = c.readPassword("Enter your old database password: ");

            if (verify(oldPassword, filepath)) {
                boolean noMatch;
//                    int i = 0;
//                    do {
                char[] newPassword1 = c.readPassword("Enter your new database password: ");
                char[] newPassword2 = c.readPassword("Enter new database password again: ");
                noMatch = !Arrays.equals(newPassword1, newPassword2);
                if (noMatch) {
                    c.format("ERROR  :  Passwords don't match. Try again.%n");
                } else {
                    boolean bResult = change(filepath, newPassword1);
                    if (bResult == true) {
                        c.format("SUCCESS  :  DB Setting Password changed successfully!!!");
                    } else {
                        c.format("ERROR  :  DB Setting Password changed failed!!!");
                    }
                }
                Arrays.fill(newPassword1, ' ');
                Arrays.fill(newPassword2, ' ');
//                        i++;
//                        if(i >= 3){
//                            noMatch = true;
//                            break;
//                        }
//                    } while (noMatch);
            } else {
                c.format("ERROR  : Old Password did not matched!!!");
            }

            Arrays.fill(oldPassword, ' ');

        } catch (Exception e) {
            e.printStackTrace();
            //out.println("<h1> DB Setting initialized failed"+ e.getMessage()+"</h1><br>");
            String strresult = "error";
            String message = "Exception::" + e.getMessage();

        }
    }

    // Dummy change method.
    static boolean verify(char[] password, String filepath) {
        try {
            PropsFileUtil p = new PropsFileUtil();

            if (p.LoadFile(filepath) == true) {

            } else {
                System.out.println("manually loaded dbsetting setting file failed to load >> " + filepath);
            }
            String oldDBEncryptedPass = p.properties.getProperty("db.password");
            String oldUsername = p.properties.getProperty("db.username");

            //System.out.println("oldDBEncryptedPass   :" + oldDBEncryptedPass);
            AES aesObj = new AES();
            String oldCurrentDBPass = aesObj.PINDecrypt(oldDBEncryptedPass, AES.getSignature());
            String oldCurrentusername = aesObj.PINDecrypt(oldUsername, AES.getSignature());
//            System.out.println("oldCurrentDBPass   :  " + oldCurrentDBPass);
//            System.out.println("oldCurrentusername   :  " + oldCurrentusername);
            String oldDBPass = new String(password);
            if (oldDBPass.equals(oldCurrentDBPass)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
        // This method always returns
        // true in this example.
        // Modify this method to verify
        // password according to your rules.

    }

    // Dummy change method.
    static boolean change(String filepath, char[] newDBPass) {

        try {
            PropsFileUtil p = new PropsFileUtil();

            if (p.LoadFile(filepath) == true) {

            } else {
                System.out.println("manually loaded dbsetting setting file failed to load >> " + filepath);
            }
            String oldDBEncryptedPass = p.properties.getProperty("db.password");
            //System.out.println("oldDBEncryptedPass   :" + oldDBEncryptedPass);
            AES aesObj = new AES();
            //System.out.println("newDBPass   :" + aesObj.PINEncrypt(new String(newDBPass), AES.getSignature()));

            p.properties.setProperty("db.password", aesObj.PINEncrypt(new String(newDBPass), AES.getSignature()));

            HashMap map = new HashMap();
            boolean bResult = false;
            Enumeration enamObj = p.properties.propertyNames();
            while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) p.properties.getProperty(key);
                map.put(key, value);
            }

            bResult = p.ReplaceProperties(map, filepath);
            return bResult;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // Modify this method to change
        // password according to your rules.
        return false;
    }
}
