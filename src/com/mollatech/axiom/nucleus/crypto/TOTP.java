package com.mollatech.axiom.nucleus.crypto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.UndeclaredThrowableException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TOTP {

    public TOTP() {
    }
    
    public String GetOTP(String seed, long otptime, String length, long t0 , int iALGO, int iStepsinSeconds){
        String hashingAlgo = "";
        if (iALGO == 1) {
            hashingAlgo = "HmacSHA1";
        }
        if (iALGO == 2) {
            hashingAlgo = "HmacSHA256";
        }
        if (iALGO == 3) {
            hashingAlgo = "HmacSHA512";
        }
            long T = (otptime - t0) / iStepsinSeconds;   //30 seconds time steps (recommended)
            String steps = "0";
            steps = Long.toHexString(T).toUpperCase();
            while (steps.length() < 16) {
                steps = "0" + steps;
            }
        String otp = generateTOTP(seed, steps, length, hashingAlgo);
        return otp;
    }

//    public String GetOTP(String seed, long otptime, String length, long t0 , int iALGO, int iStepsinSeconds) {
//        try {
//            long T = (otptime - t0) / iStepsinSeconds;   //30 seconds time steps (recommended)
//            String steps = "0";
//            steps = Long.toHexString(T).toUpperCase();
//            while (steps.length() < 16) {
//                steps = "0" + steps;
//            }
//
//            String otp = null;
//            if (iALGO == 1) {
//                otp = generateTOTP(seed, steps, length, "HmacSHA1");
//            } else
//            if (iALGO == 2) {
//                otp = generateTOTP256(seed, steps, length);
//            } else
//            if (iALGO == 3) {
//                otp = generateTOTP512(seed, steps, length);
//            }
//
//            //String s = LoadSettings.g_sSettings.getProperty("debug");
//            String s = "no";
//            if ( s.compareToIgnoreCase("yes") == 0 ) {
//                System.out.println("GetOTP::seed::" + seed);
//                System.out.println("GetOTP::otptime::" + otptime);
//                System.out.println("GetOTP::length::" + length);
//                System.out.println("GetOTP::t0::" + t0);
//                System.out.println("GetOTP::OTP::" + otp);
//            }
//
//            return otp;
//        } catch (Exception e) {
//            System.out.println("GetOTP::Exception" + e.getMessage());
//            return null;
//        }
//
//    }
//
//    public boolean VerifyOTP(String seed, String otp, String length, long t0 ,  int iALGO, int iStepsinSeconds,int iSteps) {
//
//        try {
//
//            long tVerifyOTP = new java.util.Date().getTime() / 1000;
//
//            //String strSteps = LoadSettings.g_sSettings.getProperty("otp.duration.steps");
//            //int iSteps = Integer.valueOf(strSteps);
//
//            int iStepDuration = 0;
//            for ( int i = 0 ; i < iSteps; i ++ ){
//                String strOTP = GetOTP(seed, tVerifyOTP - iStepDuration, length, t0, iALGO,iStepsinSeconds );       //time step 0
//                if (otp.compareTo(strOTP) == 0) {
//                    return true;
//                }
//                iStepDuration += 30;
//            }
//
//            iStepDuration = 30;
//            for ( int i = 1 ; i < iSteps; i ++ ){
//                String strOTP = GetOTP(seed, tVerifyOTP + iStepDuration, length, t0 , iALGO, iStepsinSeconds);       //time step 0
//                if (otp.compareTo(strOTP) == 0) {
//                    return true;
//                }
//                iStepDuration += 30;
//            }
//
//
////            String[] strOTPS = new String[7];
////            strOTPS[0] = GetOTP(seed, tVerifyOTP, length, t0);       //time step 0
////            if (otp.compareTo(strOTPS[0]) == 0) {
////                return true;
////            }
////            strOTPS[1] = GetOTP(seed, tVerifyOTP - 30, length, t0);     //time step 1
////            if (otp.compareTo(strOTPS[1]) == 0) {
////                return true;
////            }
////            strOTPS[2] = GetOTP(seed, tVerifyOTP - 60, length, t0);    //time step 2
////            if (otp.compareTo(strOTPS[2]) == 0) {
////                return true;
////            }
////            strOTPS[3] = GetOTP(seed, tVerifyOTP - 90, length, t0);   //time step 3
////            if (otp.compareTo(strOTPS[3]) == 0) {
////                return true;
////            }
////            strOTPS[4] = GetOTP(seed, tVerifyOTP + 30, length, t0);    //time step -1
////            if (otp.compareTo(strOTPS[4]) == 0) {
////                return true;
////            }
////            strOTPS[5] = GetOTP(seed, tVerifyOTP + 60, length, t0);   //time step -2
////            if (otp.compareTo(strOTPS[5]) == 0) {
////                return true;
////            }
////            strOTPS[6] = GetOTP(seed, tVerifyOTP + 90, length, t0);   //time step -3
////            if (otp.compareTo(strOTPS[6]) == 0) {
////                return true;
////            }
//            return false;
//        } catch (Exception e) {
//            System.out.println("GetOTP::Exception::" + e.getMessage());
//            return false;
//        }
//
//    }

    /**
     * This method uses the JCE to provide the crypto algorithm.
     * HMAC computes a Hashed Message Authentication Code with the
     * crypto hash algorithm as a parameter.
     *
     * @param crypto: the crypto algorithm (HmacSHA1, HmacSHA256,
     *                             HmacSHA512)
     * @param keyBytes: the bytes to use for the HMAC key
     * @param text: the message or text to be authenticated
     */
    private byte[] hmac_sha(String crypto, byte[] keyBytes,
            byte[] text) {
        try {
            Mac hmac;
            hmac = Mac.getInstance(crypto);
            SecretKeySpec macKey =
                    new SecretKeySpec(keyBytes, "RAW");
            hmac.init(macKey);
            return hmac.doFinal(text);
        } catch (Exception gse) {
            throw new UndeclaredThrowableException(gse);
        }
    }

    /**
     * This method converts a HEX string to Byte[]
     *
     * @param hex: the HEX string
     *
     * @return: a byte array
     */
    private byte[] hexStr2Bytes(String hex) {
        // Adding one byte to get the right conversion
        // Values starting with "0" can be converted
        byte[] bArray = new BigInteger("10" + hex, 16).toByteArray();

        // Copy all the REAL bytes, not the "first"
        byte[] ret = new byte[bArray.length - 1];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = bArray[i + 1];
        }
        return ret;
    }
    private final int[] DIGITS_POWER // 0 1  2   3    4     5      6       7        8
            = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};

    /**
     * This method generates a TOTP value for the given
     * set of parameters.
     *
     * @param key: the shared secret, HEX encoded
     * @param time: a value that reflects a time
     * @param returnDigits: number of digits to return
     *
     * @return: a numeric String in base 10 that includes
     *              {@link truncationDigits} digits
     */
    public String generateTOTP(String key,
            String time,
            String returnDigits) {
        return generateTOTP(key, time, returnDigits, "HmacSHA1");
    }

    /**
     * This method generates a TOTP value for the given
     * set of parameters.
     *
     * @param key: the shared secret, HEX encoded
     * @param time: a value that reflects a time
     * @param returnDigits: number of digits to return
     *
     * @return: a numeric String in base 10 that includes
     *              {@link truncationDigits} digits
     */
    public String generateTOTP256(String key,
            String time,
            String returnDigits) {
        return generateTOTP(key, time, returnDigits, "HmacSHA256");
    }

    /**
     * This method generates a TOTP value for the given
     * set of parameters.
     *
     * @param key: the shared secret, HEX encoded
     * @param time: a value that reflects a time
     * @param returnDigits: number of digits to return
     *
     * @return: a numeric String in base 10 that includes
     *              {@link truncationDigits} digits
     */
    public String generateTOTP512(String key,
            String time,
            String returnDigits) {
        return generateTOTP(key, time, returnDigits, "HmacSHA512");
    }

    /**
     * This method generates a TOTP value for the given
     * set of parameters.
     *
     * @param key: the shared secret, HEX encoded
     * @param time: a value that reflects a time
     * @param returnDigits: number of digits to return
     * @param crypto: the crypto function to use
     *
     * @return: a numeric String in base 10 that includes
     *              {@link truncationDigits} digits
     */
    public String generateTOTP(String key,
            String time,
            String returnDigits,
            String crypto) {
        
//        System.out.println("Crypto Algo"+crypto);
//        System.out.println("time  "+time);
        
        int codeDigits = Integer.decode(returnDigits).intValue();
        String result = null;

        // Using the counter
        // First 8 bytes are for the movingFactor
        // Compliant with base RFC 4226 (HOTP)
        while (time.length() < 16) {
            time = "0" + time;
        }
//        System.out.println("Time " + time);
        // Get the HEX in a Byte[]
        byte[] msg = hexStr2Bytes(time);
        byte[] k = hexStr2Bytes(key);
//        System.out.println("k" + k.length);
//        System.out.println("crypto"+crypto);
        byte[] hash = hmac_sha(crypto, k, msg);
//        System.out.println("hmac_sha hash" + new String(hash));
//        System.out.println("hmac_sha hash Length" + hash.length);
        // put selected bytes into result int
        int offset = hash[hash.length - 1] & 0xf;
//        System.out.println("offset" + offset);
        int binary =
                ((hash[offset] & 0x7f) << 24)
                | ((hash[offset + 1] & 0xff) << 16)
                | ((hash[offset + 2] & 0xff) << 8)
                | (hash[offset + 3] & 0xff);
//        System.out.println("Binary"+binary);
        int otp = binary % DIGITS_POWER[codeDigits];

        result = Integer.toString(otp);
        while (result.length() < codeDigits) {
            result = "0" + result;
        }
//        System.out.println("OTP "+otp);
        return result;
    }
    
//    private static String GetUserInput(String strAsk) {
//        //  prompt the user to enter their name
//        System.out.print(strAsk);
//
//        //  open up standard input
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//
//        String userName = null;
//
//        //  read the username from the command-line; need to use try/catch with the
//        //  readLine() method
//        try {
//            userName = br.readLine();
//        } catch (IOException ioe) {
//            System.out.println("IO error trying to read your name!");
//            //System.exit(1);
//        }
//
//        //System.out.println("Thanks for the name, " + userName);
//        return userName;
//
//
//    }
    

//    public static void main(String[] args) throws InterruptedException {
//        
//        String seed = "ABB6C7BA008C9AF95D7B423E09EABB5B687478E4";
//        
//        TOTP totp = new TOTP();
//        
//        for(int i=0;i<1;i++){
//            try {
//                long tOTP = new java.util.Date().getTime()/1000;
//                String otp = totp.GetOTP(seed,tOTP, "6",0,1,60);
//                Date d = new Date(tOTP*1000);
//                System.out.println("OTP::" +d.toString() + " and OTP is " + otp);
//                //String t = GetUserInput("" + i + "=" );
//                
//                long tOTPOff = 60;
//                String otpV = totp.GetOTP(seed,tOTP+tOTPOff, "6",0,1,60);
//                d = new Date((tOTP + tOTPOff)*1000);
//                System.out.println("OTP + 60 ::" + d.toString() + " and OTP is " + otpV);
//                
//                tOTPOff = 120;
//                otpV = totp.GetOTP(seed,tOTP+tOTPOff, "6",0,1,60);
//                d = new Date((tOTP + tOTPOff)*1000);
//                System.out.println("OTP + 120 ::" + d.toString() + " and OTP is " + otpV);
//                
//                tOTPOff = -60;
//                d = new Date((tOTP + tOTPOff)*1000);
//                otpV = totp.GetOTP(seed,tOTP + tOTPOff, "6",0,1,60);
//                System.out.println("OTP - 60 ::" + d.toString() + " and OTP is " + otpV);
//                                                
//                tOTPOff = -120;
//                d = new Date((tOTP + tOTPOff)*1000);
//                otpV = totp.GetOTP(seed,tOTP+tOTPOff, "6",0,1,60);
//                System.out.println("OTP - 120 ::" + d.toString() + " and OTP is " + otpV);
//                
//                //System.out.println("Verify OTP::" +d.toString() + " and OTP is " + otp);
//                //String t = GetUserInput("" + i + "=" );
//                
//                
//                
//            }
//            catch (Exception ex) {
//                Logger.getLogger(TOTP.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            //        String seed = "ABB6C7BA008C9AF95D7B423E09EABB5B687478E4";
//            //        //String seed = "3132333435363738393031323334353637383930";
//            //        //String seed = "CC70B4018D4A21C7C7F2F8752B3527444E02679B";
//            //
//            //        //long l = Long.valueOf("1331485147").longValue();
//            //        //long l = Long.valueOf("1340095728").longValue();
//            //        //long l = Long.valueOf("0").longValue();
//            //        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            //        //df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
//            //        long tOTP = new java.util.Date().getTime()/1000;
//            //        //long tOTP = 1340987906;
//            //        //long tOTP = 59;
//            //        TOTP totp = new TOTP();
//            //        String otp = totp.GetOTP(seed,tOTP, "6",0,1,60);
//            //        System.out.println("Generate OTP::" +tOTP + " and SHA 1 OTP is " + otp);
//            ////         otp = totp.GetOTP(seed,tOTP, "6",l,2,30);
//            ////        System.out.println("Generate OTP::" +tOTP + " and SHA 256 OTP is " + otp);
//            ////         otp = totp.GetOTP(seed,tOTP, "6",l,3,30);
//            ////        System.out.println("Generate OTP::" +tOTP + " and SHA 512 OTP is " + otp);
//            //
//            //
//            //        for ( int i = 0 ; i < 10 ; i++) {
//            //
//            //            long tOTP1 = (new java.util.Date().getTime()/1000)+60000;
//            //            TOTP totp1 = new TOTP();
//            //            String otp1 = totp1.GetOTP(seed,tOTP1, "6",0,1,60);
//            //            System.out.println("Generate OTP::" +tOTP1 + " and OTP is " + otp1);
//            //
//            //
//            //
//            //            //String otp1 = "=037866";
//            ////            boolean bVerifyOTP = totp.VerifyOTP( seed, otp, "6" , l);
//            ////            System.out.println( i + ":verify OTP::" + bVerifyOTP);
//            ////            Thread.sleep(60000);
//            //        }
//            
//        }
        
        
           
        
        
           
//        String seed = "ABB6C7BA008C9AF95D7B423E09EABB5B687478E4";
//        //String seed = "3132333435363738393031323334353637383930";
//        //String seed = "CC70B4018D4A21C7C7F2F8752B3527444E02679B";
//
//        //long l = Long.valueOf("1331485147").longValue();
//        //long l = Long.valueOf("1340095728").longValue();
//        //long l = Long.valueOf("0").longValue();
//        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        //df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
//        long tOTP = new java.util.Date().getTime()/1000;
//        //long tOTP = 1340987906;
//        //long tOTP = 59;
//        TOTP totp = new TOTP();
//        String otp = totp.GetOTP(seed,tOTP, "6",0,1,60);
//        System.out.println("Generate OTP::" +tOTP + " and SHA 1 OTP is " + otp);
////         otp = totp.GetOTP(seed,tOTP, "6",l,2,30);
////        System.out.println("Generate OTP::" +tOTP + " and SHA 256 OTP is " + otp);
////         otp = totp.GetOTP(seed,tOTP, "6",l,3,30);
////        System.out.println("Generate OTP::" +tOTP + " and SHA 512 OTP is " + otp);
//        
//
//        for ( int i = 0 ; i < 10 ; i++) {
//
//            long tOTP1 = (new java.util.Date().getTime()/1000)+60000;
//            TOTP totp1 = new TOTP();
//            String otp1 = totp1.GetOTP(seed,tOTP1, "6",0,1,60);
//            System.out.println("Generate OTP::" +tOTP1 + " and OTP is " + otp1);
//            
//
//
//            //String otp1 = "=037866";
////            boolean bVerifyOTP = totp.VerifyOTP( seed, otp, "6" , l);
////            System.out.println( i + ":verify OTP::" + bVerifyOTP);
////            Thread.sleep(60000);
//        }
    }

//}
