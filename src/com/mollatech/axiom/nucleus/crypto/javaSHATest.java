package com.mollatech.axiom.nucleus.crypto;


import java.security.MessageDigest;

public class javaSHATest {

    private static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }

//    public static void main(String[] args) {
//
//        try {
//            MessageDigest md = MessageDigest.getInstance("SHA1");
//            md.update("test1234".getBytes());
//            byte[] output = md.digest();
//            //byte[] half_output = new byte[20];
//            //System.arraycopy(output, 0, half_output, 0, 16);
//            String strHexSecret = bytesToHex(output);
//            System.out.println("SHA-1(\"test1234\") is :"  +strHexSecret);
//            System.out.println("***********");
//
//            md = MessageDigest.getInstance("SHA-256");
//            md.update("test1234".getBytes());
//            output = md.digest();
//            //byte[] half_output = new byte[20];
//            //System.arraycopy(output, 0, half_output, 0, 16);
//            strHexSecret = bytesToHex(output);
//            System.out.println("SHA-256(\"test1234\") is :"  +strHexSecret);
//            
//        } catch (Exception e) {
//            System.out.println("Error : " + e);
//        }
//    }
}


