/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.nucleus.crypto;

import java.io.Serializable;

/**
 *
 * @author Ideasventure
 */
public class ChannelProfile implements Serializable {

    private static final long serialVersionUID = 599901307498275970L;
    public final static int _CHECKUSERBYNAME = 1;
    public final static int _CHECKUSERBYEMAILPHONEUSERID = 2;
    public final static int _NOUSERALERT = 0;
    public final static int _USERALERT = 1;
    //public final static int _CHECKSESSION = 0;  
    //public final static int _NOCHECKSESSION = -1; 

    public final static String _ALERTMEDIASMS = "sms";
    public final static String _ALERTMEDIAEMAIL = "email";
    public final static String _ALERTMEDIAVOICE = "voice";
    public final static String _ALERTMEDIAUSSD = "ussd";

    public final static String _SWOTPTYPESIMPLE = "simple";
    public final static String _SWOTPTYPEMOBILE = "mobiletrust";

    public final static String tokensettingloadyes = "yes";
    public final static String tokensettingloadno = "no";

    public final static int _RESETUSERTOACTIVE = 1;
    public final static int _RESETUSERTOUNASSIGN = 0;

    public final static int _AUTHARIZATIONSTATUSTOACTIVE = 1;
    public final static int _AUTHARIZATIONSTATUSTOINACTIVE = 0;

    
    public final static int _AUTHARIZATIONBYSAMEUNIT = 1;
    public final static int _AUTHARIZATIONBYDIFFRENTUNIT = 2;

    public final static int CONNECTORSTATUSFIVEMINS = 300;
    public final static int CONNECTORSTATUSTENMINS = 600;
    public final static int CONNECTORSTATUSTHIRTYMINS = 1800;
    public final static int CONNECTORSTATUSONEHOUR = 3600;
    public final static int CONNECTORSTATUSTHREEHOUR = 10800;

    public final static int CLEANUPDAYSNINETY = 90;
    public final static int CLEANUPDAYSONEEIGHTY = 180;
    public final static int CLEANUPDAYSTWOSEVENTY = 270;
    public final static int CLEANUPDAYSTHREESIXTYFOUR = 364;
    public final static int CLEANUPDAYSSEVENTWENYEIGHT = 728;
    public  boolean _HardWareTokenUnitAutherization;
    public int resetUser;
    public int connectorStatus;
    public int checkUser;
    public int alertUSer;
    public int cleanupdays;
    public boolean deleteUser;
    public boolean editUser;
    public String tokensettingload;
    //public int checksession;
    public String alertmedia;
    public String swotptype;

    public String cleanuppath;
    public String remotesignarchive;
    public String bulkemailattachment;

//    public String pfxfilepath;
//    public String pfxpassword;
    public String locationclassname;
    public String otpspecification;
    public String certspecification;
    public String signspecification;

    public int authorizationStatus;
    public int authorizationDuration;
    public int authorizationunit;

    public int _dayRestriction;
    public int _timeFromInHour;
    public int _timeToInHour;
    public int _timfromampm;
    public int _timetoampm;
    public int _multipleSession;
    public int _serverRestriction;
    
//    public int _userValidityDays;
    
    
//     public int _sweetSpotDeviation;
//    public int _xDeviation;
//    public int _yDeviation;
}
