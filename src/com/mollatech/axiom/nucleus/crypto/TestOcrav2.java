/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.nucleus.crypto;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author vikramsareen
 */
public class TestOcrav2 {

    public static int GetToday() throws ParseException {
  
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date auditDate = df.parse("2016-12-31");

        try {
            Calendar current = Calendar.getInstance();
            current.setTime(auditDate);
            current.set(Calendar.AM_PM, Calendar.AM);

            current.set(Calendar.HOUR, 24);
            current.set(Calendar.MINUTE, 0);
            current.set(Calendar.SECOND, 0);
            current.set(Calendar.MILLISECOND, 0);

            Date startDate = current.getTime();

//            Calendar current1 = Calendar.getInstance();
//            current1.setTime(auditDate);
//            current1.set(Calendar.AM_PM, Calendar.AM);
//            current1.set(Calendar.HOUR, -24);
//            current1.set(Calendar.MINUTE, 0);
//            current1.set(Calendar.SECOND, 0);
//            current1.set(Calendar.MILLISECOND, 0);
//            
//            Date endDate = current1.getTime();
           // System.out.println(auditDate);
           // System.out.println(startDate);
            //System.out.println(endDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return -100;
    }

    public static String numStrToHex(String question) {
        String qHex = new String((new BigInteger(question, 10))
                .toString(16)).toUpperCase();
 
        return qHex;
    }
    
    
//    public static String getHash(String data)
//    {
//         byte[] hashedData=null;
//        try{
//         MessageDigest md = MessageDigest.getInstance("SHA1");
//            md.update(data.getBytes());
//         hashedData = md.digest();
//        }catch(Exception ex)
//        {
//        ex.printStackTrace();
//        }
//        return Base64.encode(hashedData);
//    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        
        String x="xTr48cXfbay43gvvEyGB+Mbjc+rwI6+8aWQhRmvmF2dsVmygFq2eqJXwu+yvsBjZ6Dsa+DXBlK+scoh7Ta1ohSCVQPejOZvvMuhp37P/bAVAs1+Q5RrBQATEVso/ks=";
        System.out.println("X"+x.length());
//            MessageDigest md = MessageDigest.getInstance("SHA1");
//            md.update("Pramod".getBytes());
//            byte[] hashedData = md.digest();
        //System.out.println(getHash(""));
        
//        try {
//            GetToday();
//        } catch (ParseException ex) {
//            Logger.getLogger(TestOcrav2.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        

        OCRA oc = new OCRA();
        String result;
         long iDrfitInMins = 16;
        Date da=new Date(System.currentTimeMillis()+iDrfitInMins*60*1000);
        System.out.println("Daa"+da.getTime());
        Date myDate = da;         // Calendar.getInstance().getTime();
        BigInteger b = new BigInteger("0");
        String timeStamp = "";
        b = new BigInteger("" + myDate.getTime());
            //added by vikram to fix out of sync time problem
       
        //iDrfitInMins = iDrfitInMins * 60*1000; //making it into seconds
        BigInteger biTimeDrift = new BigInteger("" + iDrfitInMins);
        //end of addition
        //jack token secret
        //"850a78e518962942e14ba548cd921b2e3f365419",

        for (int i = 0; i < 5; i++) {
            String inc = "" + (i * 60000);

            b = b.add(new BigInteger(inc));

           // b = b.add(biTimeDrift);

            BigInteger b1 = b.divide(new BigInteger("60000"));
            timeStamp = numStrToHex(b1.toString());

            result = oc.generateOCRA(
                    "OCRA-1:HOTP-SHA1-6:QN64-T1M",                    
                    "f43a26706e5fd188e07b49a85882c189929dc81c",
                    "",
                    numStrToHex("123456"),
                    "", "", timeStamp);
            System.out.println("Time was =" + timeStamp+", Result=" + result);
        }

        for (int i = 5; i > 0; i--) {
            String inc = "" + (i * 60000);

            b = b.subtract(new BigInteger(inc));

            b = b.add(biTimeDrift);

            BigInteger b1 = b.divide(new BigInteger("60000"));
            timeStamp = numStrToHex(b1.toString());

            result = oc.generateOCRA("OCRA-1:HOTP-SHA1-6:QN64-T1M",
                    "e0b1cf2f5df5066f3e5db46b513634bd8e4c6286",
                    "",
                    numStrToHex("397154"),
                    "", "", timeStamp);
            System.out.println("Time was =" + timeStamp+", Result=" + result);
        }

    }

}
