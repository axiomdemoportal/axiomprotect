package com.mollatech.axiom.nucleus.crypto;

import com.license4j.HardwareID;
import com.license4j.LicenseFile;
import com.license4j.LicenseValidator;
import com.license4j.exceptions.LicenseContentException;
import com.license4j.exceptions.LicenseFileReadException;
import com.license4j.exceptions.LicenseFileSecurityException;
import com.license4j.exceptions.LicenseHardwareIDException;
import com.license4j.exceptions.LicenseSecurityException;
import com.mollatech.axiom.mobiletrust.crypto.CryptoManager;
import com.mollatech.axiom.mobiletrust.crypto.MobileTrustServerLicenseAttributes;
import static com.mollatech.axiom.nucleus.crypto.HWSignature.asHex;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AxiomProtect {

    private static String licfile = null;
    private static String publickey = null;
    private static String aeskey = null;
    //private static String hardwareid = null;
    private static Properties g_sSettings = null;
    private static byte[] aes_password = null;
    private static String g_filepath = null;

    private static Date g_sCheckDateForLicense = null;

    public static String USER_PASSWORD = "user.password";
    public static String USER_CHALLENGERESPONSE = "user.channelresponse";
    public static String OTP_OOB = "otp.oob";
    public static String OTP_SOFTWARE = "otp.software";
    public static String OTP_HARDWARE = "otp.hardware";
    public static String PKI_CERTIFICATE = "pki.certificate";
    public static String PKI_HARDWARE = "pki.hardware";
    public static String PKI_SOFTWARE = "pki.software";

    public static String MOBILETRUST_DEVICEPROFILE = "mobile.device.profile";
    public static String MOBILETRUST_GEOFENCE = "mobile.trust.geo";
    public static String MOBILETRUST_PUSH = "mobile.trust.push";
    public static String MOBILETRUST_E2E = "mobile.trust.e2e";
    public static String MOBILETRUST_TIMESTAMP = "mobile.trust.timestamp";
    public static String MOBILETRUST_TYPE = "mobiletrust.licensetype";
    public static String MOBILETRUST = "mobile.trust";

    public static String REMOTESIGNSERVICEWITH_PASSWORD = "remote.signing.service.password";
    public static String REMOTESIGNSERVICEWITH_OTP = "remote.signing.service.otp";
    public static String REMOTESIGNSERVICEWITH_CHALLENGERESPONSE = "remote.signing.service.channelresponse";

    public static String EPIN = "epin";

    public static final String CHANNELS = "channels";
    public static final String OPERATORS = "operators";
    public static final String GATEWAY_SMS = "gateway.sms";
    public static final String GATEWAY_EMAIL = "gateway.email";
    public static final String GATEWAY_VOICE = "gateway.voice";
    public static final String GATEWAY_USSD = "gateway.ussd";
    public static final String GATEWAY_FAX = "gateway.fax";

    public static final String INTERACTIONS = "interactions";
    public static final String GATEWAY_PUSH_APPLE = "gateway.push.apple";
    public static final String GATEWAY_PUSH_GOOGLE = "gateway.push.google";
    public static final String THIRD_PARTY_CALLERS_COUNT = "callers.count";
    public static final String THIRD_PARTY_CALLERS = "callers";
    public static final String SOCIAL_MESSAGING = "social.messaging";

    // New Licesing Variable here
    public static final String USERS_COUNT = "users.count";
    public static final String SOFTWARETOKEN_COUNT = "softwaretoken.count";
    public static final String HARDWARETOKEN_COUNT = "hardwaretoken.count";
    public static final String OOBTOKEN_COUNT = "oobtoken.count";
    
    public static final String BILLING_MANAGER_SETTINGS = "billing.manager";
    public static final String IMAGE_AUTH = "image.auth";
    public static final String TWO_WAY_AUTH = "two.way.auth";
    public static final String WEB_WATCH = "web.watch";
    public static final String WEB_WATCH_COUNT = "web.watch.count";
    public static final String WEB_RESOURCES = "secure.browser.allowed";
    public static final String WEB_RESOURCES_COUNT = "secure.browser.resource.count";
    public static final String REPORT_SCHEDULER = "report.scheduler";
    public static final String CERT_DISCOVERY_ENABLED="certificate.discovery.enabled";
    public static final String CERT_DISCOVERY_IPCOUNT="certificate.discovery.count";
    public static final String CERTIFICATE_MANAGEMENT="ssl.certificate.management.enabled";
    public static final String SSL_CERTIFICATE_COUNT  ="ssl.certificate.count";
    public static final String EXTERNAL_SYSTEM_REPORTS="external.system.report.enabled";
    
    static {
        LoadAxiomProtect();
        ValidateLicense();

    }

    private static void LoadAxiomProtect() {
        try {

            String sep = System.getProperty("file.separator");
            String usrhome = System.getProperty("catalina.home");

            if (usrhome == null) {
                usrhome = System.getenv("catalina.home");
            }

            if (usrhome == null) {
                usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
            }

//            usrhome = "E:\\Manoj New\\apache-tomcat-7.0.53\\apache-tomcat-7.0.53";
            usrhome += sep + "axiomv2-settings";

            g_filepath = usrhome + sep + "dbsetting.conf";

            //g_filepath = "/Applications/NetBeans/apache-tomcat-7.0.27/axiomv2-settings/dbsetting.conf";
            PropsFileUtil p = new PropsFileUtil();

            if (p.LoadFile(g_filepath) == true) {
                g_sSettings = p.properties;
            } else {
               // System.out.println("license  file failed to load >> " + g_filepath);
            }
            aeskey = g_sSettings.getProperty("reserved.1");
            String strHashKey = g_sSettings.getProperty("reserved.7");
            publickey = g_sSettings.getProperty("reserved.2");

            licfile = usrhome + sep + "license.enc";

            if (aeskey != null && aeskey.isEmpty() == false) {
                AES aesObj = new AES();
                //System.out.println("reserved.7" + strHashKey);
                //System.out.println("key to decrypt" + AES.getSignature());
                byte[] byteHashKey = aesObj.PINDecryptBytes(strHashKey, AES.getSignature());
                //System.out.println("decrypted key " + asHex(byteHashKey));
                //System.out.println("encrypted random key " + aeskey);
                aes_password = aesObj.PINDecryptBytes(aeskey, asHex(byteHashKey));
             
            } else {
              //  System.out.println("ERROR: KEY is NULL... installation is tempered or corrupted!!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int SetPasswordsInner(String[] passwords) {
        try {
            if (passwords == null || passwords.length == 0) {
                return -3;
            }

            MessageDigest md = MessageDigest.getInstance("SHA1");
            for (int i = 0; i < passwords.length; i++) {
                md.update(passwords[i].getBytes());
            }
            byte[] outputHashOFCredentials = md.digest();

            md.reset();

            AES aesObj = new AES();
            String password = AES.getSignature();
            String strEncryptedKEY = aesObj.PINEncryptBytes(outputHashOFCredentials, password);

            String password2 = asHex(outputHashOFCredentials);
            byte[] dbPassword = GenerateRandomNumber().getBytes();
            String strEndDBPassword = aesObj.PINEncryptBytes(dbPassword, password2);

            PropsFileUtil pfUtilsObj = new PropsFileUtil();
            boolean bResult = false;
            bResult = pfUtilsObj.LoadFile(g_filepath);
            HashMap map = new HashMap();

            pfUtilsObj.properties.setProperty("reserved.7", strEncryptedKEY);
            pfUtilsObj.properties.setProperty("reserved.1", strEndDBPassword);

            Enumeration enamObj = pfUtilsObj.properties.propertyNames();
            while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) pfUtilsObj.properties.getProperty(key);
                map.put(key, value);
            }

            bResult = pfUtilsObj.ReplaceProperties(map, g_filepath);
            if (bResult == true) {
                LoadAxiomProtect();
                return 0;
            } else {
                return -2;
            }

            //return strEncryptedData;
            //return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    private static int ReplacePasswordsInner(String[] curpasswords, String[] newpasswords) {
        try {

            if (curpasswords == null || curpasswords.length == 0) {
                return -4;
            }

            if (newpasswords == null || newpasswords.length == 0) {
                return -3;
            }

            MessageDigest md = MessageDigest.getInstance("SHA1");
            for (int i = 0; i < curpasswords.length; i++) {
                md.update(curpasswords[i].getBytes());
            }
            byte[] outputCurrentAESKEY = md.digest();

            PropsFileUtil pfUtilsObj = new PropsFileUtil();
            boolean bResult = pfUtilsObj.LoadFile(g_filepath);
            HashMap map = new HashMap();

            String strCurPassInHex = pfUtilsObj.properties.getProperty("reserved.7");

            AES aesObj = new AES();
            byte[] bytesDecryptedCurPassword = aesObj.PINDecryptBytes(strCurPassInHex, AES.getSignature());

            String CurPasswordFromFile = asHex(bytesDecryptedCurPassword);
            String CurPasswordFromDashbd = asHex(outputCurrentAESKEY);

            if (CurPasswordFromDashbd.compareTo(CurPasswordFromFile) != 0) {
                //password did not match
                Date d = new Date();

               // System.out.println(d + ">>" + "Failed to match");
                return -9;
            }

            md.reset();
            for (int i = 0; i < newpasswords.length; i++) {
                md.update(newpasswords[i].getBytes());
            }
            byte[] outputNewHashFromOfficers = md.digest();
            String strEncryptedKEY = aesObj.PINEncryptBytes(outputNewHashFromOfficers, AES.getSignature());

            pfUtilsObj.properties.setProperty("reserved.7", strEncryptedKEY);

            //replace the r.1 also 
            String strEncRandomKey = pfUtilsObj.properties.getProperty("reserved.1");
            //decrypt the r.1 using old hash key 
            //byte[] strPlainRandomKey = aesObj.PINDecryptBytes(strEncRandomKey, new String(bytesDecryptedCurPassword));
            byte[] strPlainRandomKey = aesObj.PINDecryptBytes(strEncRandomKey, asHex(bytesDecryptedCurPassword));
            //encrypt the r.1 using new hash key 
            //String strEncNewRandomKey = aesObj.PINEncryptBytes(strPlainRandomKey, new String(outputNewHashFromOfficers));
            String strEncNewRandomKey = aesObj.PINEncryptBytes(strPlainRandomKey, asHex(outputNewHashFromOfficers));

            pfUtilsObj.properties.setProperty("reserved.1", strEncNewRandomKey);

            Enumeration enamObj = pfUtilsObj.properties.propertyNames();
            while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) pfUtilsObj.properties.getProperty(key);
                map.put(key, value);
            }

            bResult = pfUtilsObj.ReplaceProperties(map, g_filepath);
            if (bResult == true) {
                LoadAxiomProtect();
                return 0;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            Logger.getLogger(AxiomProtect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    private static int g_sLicenseResult = -1;

//    public static int ValidateLicense() {
//        try {
//            
//            if ( licfile == null ) 
//                return -100;
//
//            if (g_sCheckDateForLicense == null) {
//                LicenseFile license = LicenseValidator.validateLicenseFile(
//                        licfile,
//                        publickey,
//                        null,
//                        null,
//                        null,
//                        null,
//                        null);
//
//                g_sCheckDateForLicense = new Date();                
//                
//                g_sLicenseResult = license.getStatusCode();
//                
//                System.out.println("if (g_sCheckDateForLicense == null)  "  + g_sLicenseResult);
//                System.out.println("if (g_sCheckDateForLicense == null)  "  + g_sCheckDateForLicense);
//                
//                return g_sLicenseResult;
//                
//            } else {
//                Date d = new Date();
//                Calendar cal = null;
//                cal = Calendar.getInstance();
//                cal.setTime(d);
//                int date = cal.get(Calendar.DATE);
//
//                cal = Calendar.getInstance();
//                cal.setTime(g_sCheckDateForLicense);
//                int date1 = cal.get(Calendar.DATE);
//
//                if (date1 == date) {
//                    
//                    System.out.println("if (date1 == date)  "  + g_sLicenseResult);
//                    System.out.println("if (date1 == date)  "  + g_sCheckDateForLicense);
//
//                    
//                    return g_sLicenseResult; //stored result
//                } else {                    
//                    LicenseFile license = LicenseValidator.validateLicenseFile(
//                            licfile,
//                            publickey,
//                            null,
//                            null,
//                            null,
//                            null,
//                            null);
//
//                    g_sCheckDateForLicense = new Date();
//                    g_sLicenseResult = license.getStatusCode();
//                    
//                    System.out.println("if (date1 != date)  "  + g_sLicenseResult);
//                    System.out.println("if (date1 != date)  "  + g_sCheckDateForLicense);
//                    
//                    return g_sLicenseResult;
//                }
//
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return -99;
//        }
//        //return 0;
//    }
    public static int ValidateLicense() {
       
//      int a =0;//to       rebypass licensing change later
//       if(a==0){
//           return 0;
//       }
        
        try {

            if (licfile == null) {
                return -100;
            }

            LicenseFile license = null;
            license = LicenseValidator.validateLicenseFile(
                    licfile,
                    publickey,
                    null,
                    null,
                    null,
                    null,
                    null);
            //System.out.println("ValidateLicense() = " + license.getStatusCode());
            return license.getStatusCode();

        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
        //return 0;
    }

    public static MobileTrustServerLicenseAttributes GetMobileTrustLicenseAttributes() {
        try {
            LicenseFile license = LicenseValidator.validateLicenseFile(
                    licfile,
                    publickey,
                    null,
                    null,
                    null,
                    null,
                    null);

            Properties p = license.getAllSignedCustomFeatures();
            MobileTrustServerLicenseAttributes mtslAttObj = new MobileTrustServerLicenseAttributes();
            mtslAttObj.productverison = license.getProductVersion();
            mtslAttObj.licenseexpiresOn = license.getLicenseExpireDate().getTime();
            String strLicenseType = p.getProperty("mobiletrust.licensetype");
            //strLicenseType = "plus"; //to be fixed later

            if (strLicenseType.compareTo("plus") == 0) {
                mtslAttObj.licensetype = 2;
            } else {
                mtslAttObj.licensetype = 1;
            }
            mtslAttObj.fetchVisibleIPURL = p.getProperty("mobiletrust.ipurl");
            mtslAttObj.fetchGeoURL = p.getProperty("mobiletrust.geourl");

            //p.getProperty("mobiletrust.licensetype");
//            licenseObject.setSignedCustomFeature("axiomprotect.userpin", "yes");
//            licenseObject.setSignedCustomFeature("axiomprotect.oobotp", "yes");
//            licenseObject.setSignedCustomFeature("axiomprotect.swotp", "yes");
//            licenseObject.setSignedCustomFeature("axiomprotect.hwotp", "yes");
//            licenseObject.setSignedCustomFeature("axiomprotect.certificate", "yes");
//            licenseObject.setSignedCustomFeature("axiomprotect.swpki", "yes");
//            licenseObject.setSignedCustomFeature("axiomprotect.hwpki", "yes");            
//            licenseObject.setSignedCustomFeature("applications", m_strApplicationCount);            
//            licenseObject.setSignedCustomFeature("mobiletrust.licensetype", "plus");
//            licenseObject.setSignedCustomFeature("mobiletrust.ipurl", "https://www.mollatech.com:8080/mobiletrust/getip");
//            licenseObject.setSignedCustomFeature("mobiletrust.geourl", "https://www.mollatech.com:8080/mobiletrust/getgeo");            
            //license.get
            return mtslAttObj;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        //return 0;
    }

    public static String GetHardwareID() {
        //String strHWIDOrg = "230007099-4b91-3e29-af72-d7a93f6e02e3";
        String strHWIDOrg = "System cannot be issued license.. it is virtual without any static profile!!!";
        String strHWIDFound = null;
        try {
            strHWIDFound = HardwareID.getHardwareIDFromMacAddressAndHostName();
        } catch (Exception e) {
            //e.printStackTrace();
        }
        if (strHWIDFound == null || strHWIDFound.isEmpty() == true) {
            try {
                strHWIDFound = HardwareID.getHardwareIDFromHostName();
            } catch (Exception e) {
                //  e.printStackTrace();
            }
            if (strHWIDFound == null || strHWIDFound.isEmpty() == true) {
                try {
                    strHWIDFound = HardwareID.getHardwareIDFromMacAddress();
                } catch (Exception e) {
                    //    e.printStackTrace();
                }
            }
        }
        if (strHWIDFound == null || strHWIDFound.isEmpty() == true) {
            strHWIDOrg = strHWIDFound;
        }
        return strHWIDFound;
    }

    public static int CheckLicense(String LicFilePath) {
        try {
            LicenseFile license = LicenseValidator.validateLicenseFile(
                    LicFilePath,
                    publickey,
                    null,
                    null,
                    null,
                    null,
                    null);

//LicenseFile.STATUS_LICENSE_VALID = 0;                      
//LicenseFile.STATUS_LICENSE_EXPIRED = 1;
//LicenseFile.STATUS_LICENSE_MAINTENANCE_EXPIRED = 2;
//LicenseFile.STATUS_PRODUCT_ID_MISMATCH = 3;
//LicenseFile.STATUS_PRODUCT_EDITION_MISMATCH = 4;
//LicenseFile.STATUS_PRODUCT_VERSION_MISMATCH = 5;
//LicenseFile.STATUS_HARDWARE_ID_MISMATCH = 6; 
            //license.get
            return license.getStatusCode();

        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public static Properties LicenseDetails() {
        try {
            LicenseFile license = LicenseValidator.validateLicenseFile(
                    licfile,
                    publickey,
                    null,
                    null,
                    null,
                    null,
                    null);

            Properties p = new Properties();
            p.setProperty("Product ID", license.getProductId());
            p.setProperty("Activated On", license.getLicenseIssueDate().toString());
            p.setProperty("Support Expires On", license.getLicenseMaintenanceExpireDate().toString());
            p.setProperty("Registrartion Profile", license.getUserHardwareID());
            p.setProperty("Company Name", license.getUserCompanyName());
            p.setProperty("Email Id", license.getUserEMail());
            p.setProperty("Telephone", license.getUserTelNumber());
            p.setProperty("License ID", license.getLicenseID());

            //new addition for more details 
            Properties pItems = license.getAllSignedCustomFeatures();

            if (LoadSettings.g_sSettings.getProperty("product.type").compareTo("1") == 0) {
                //dictum
                p.setProperty("Interactions", pItems.getProperty(INTERACTIONS));
                p.setProperty("Apple Push Gateway Connector", pItems.getProperty(GATEWAY_PUSH_APPLE));
                p.setProperty("Google Push Gateway Connector", pItems.getProperty(GATEWAY_PUSH_GOOGLE));
                p.setProperty("3rd Party Callers", pItems.getProperty(THIRD_PARTY_CALLERS_COUNT));
                p.setProperty("3rd Party Caller Messages", pItems.getProperty(THIRD_PARTY_CALLERS));
                p.setProperty("Social (Facebook,linkedin,Twitter) Connector", pItems.getProperty(SOCIAL_MESSAGING));

            } else if (LoadSettings.g_sSettings.getProperty("product.type").compareTo("3") == 0) {
                //axiom 
                p.setProperty("User Password", pItems.getProperty(USER_PASSWORD));
                p.setProperty("User Challenge Response", pItems.getProperty(USER_CHALLENGERESPONSE));
                p.setProperty("OTP Out Of Band Token", pItems.getProperty(OTP_OOB));
                p.setProperty("OTP Software Token", pItems.getProperty(OTP_SOFTWARE));
                p.setProperty("OTP Hardware Token", pItems.getProperty(OTP_HARDWARE));
                p.setProperty("PKI Certificate", pItems.getProperty(PKI_CERTIFICATE));
                p.setProperty("PKI Hardware Token", pItems.getProperty(PKI_HARDWARE));
                p.setProperty("PKI Software Token", pItems.getProperty(PKI_SOFTWARE));
//                p.setProperty("SOTP Out Of Band Token", "no");
//                p.setProperty("SOTP Software Token", "no");
//                p.setProperty("SOTP Hardware Token", "no");
                p.setProperty("Mobile Trust Device Profile", pItems.getProperty(MOBILETRUST_DEVICEPROFILE));
                p.setProperty("Mobile Trust Geo Fencing", pItems.getProperty(MOBILETRUST_GEOFENCE));
                p.setProperty("Mobile Trust Push Notification", pItems.getProperty(MOBILETRUST_PUSH));
                p.setProperty("Mobile Trust End To End Encryption", pItems.getProperty(MOBILETRUST_E2E));
                p.setProperty("Mobile Trust Time Stamping", pItems.getProperty(MOBILETRUST_TIMESTAMP));
                p.setProperty("Mobile Trust License Type", pItems.getProperty(MOBILETRUST_TYPE));
                p.setProperty("Mobile Trust Enabled", pItems.getProperty(MOBILETRUST));
                p.setProperty("PKI Remote Server Signing (with Password)", pItems.getProperty(REMOTESIGNSERVICEWITH_PASSWORD));
                p.setProperty("PKI Remote Server Signing (with One Time Password)", pItems.getProperty(REMOTESIGNSERVICEWITH_OTP));
                p.setProperty("PKI Remote Server Signing (with Challenge Response)", pItems.getProperty(REMOTESIGNSERVICEWITH_CHALLENGERESPONSE));
                p.setProperty("E-PIN Enabled", pItems.getProperty(EPIN));
            }

            p.setProperty("Channels Allowed per Site", pItems.getProperty(CHANNELS));
            p.setProperty("Operator Allowed per Site", pItems.getProperty(OPERATORS));
            p.setProperty("SMS Gateway Connector", pItems.getProperty(GATEWAY_SMS));
            p.setProperty("Email Gateway Connector", pItems.getProperty(GATEWAY_EMAIL));
            p.setProperty("Voice Gateway Connector", pItems.getProperty(GATEWAY_VOICE));
            p.setProperty("USSD Gateway Connector", pItems.getProperty(GATEWAY_USSD));
            p.setProperty("E-FAX Gateway Connector", pItems.getProperty(GATEWAY_FAX));
            if(pItems.getProperty(EXTERNAL_SYSTEM_REPORTS)!=null){
            p.setProperty("External Reports Allowed", pItems.getProperty(EXTERNAL_SYSTEM_REPORTS));
            }
            p.setProperty("User Count", pItems.getProperty(USERS_COUNT));
            //end of new addition 

            //p.setProperty("Registered Name", license.getLicenseRegisterName());
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean IsSecurityInitialized() {
        if (aeskey == null || aeskey.isEmpty() == true) {
            return false;
        } else {
            return true;
        }
    }

    public static int SetPasswords(String[] passwords) {
        return SetPasswordsInner(passwords);
    }

    public static int ReplacePasswords(String[] curpasswords, String[] newpasswords) {
        return ReplacePasswordsInner(curpasswords, newpasswords);
    }

    public static byte[] AccessDataBytes(byte[] bytesEncryptedData) {
        try {
            if (bytesEncryptedData == null) {
                return null;
            }

            //System.out.println("AccessDataBytes data size is " + bytesEncryptedData.length);
            //return bytesEncryptedData;
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.decryptAES(aeskey1, bytesEncryptedData);
            return original;
//            raw = Arrays.copyOf(aes_password, 16);
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            //Cipher cipher = Cipher.getInstance("AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//            byte[] original = cipher.doFinal(bytesEncryptedData);
//            return original;
            //return bytesEncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //return bytesEncryptedData;
        }
    }

    public static String AccessData(String strEncryptedData) {
        try {

            if (strEncryptedData == null || strEncryptedData.isEmpty() == true) {
                return null;
            }

            byte[] hexBytes = HWSignature.hex2Byte(strEncryptedData);

            //return strEncryptedData;
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.decryptAES(aeskey1, hexBytes);
            return new String(original);

//            byte[] hexBytes = HWSignature.hex2Byte(strEncryptedData);
//            byte[] raw = aes_password;
//            raw = Arrays.copyOf(aes_password, 16);
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//            byte[] original = cipher.doFinal(hexBytes);
//            String originalString = new String(original);
//            return originalString;
            //return strEncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //return strEncryptedData;
        }
    }

    public static String ProtectData(String strData) {
        try {

            if (strData == null || strData.isEmpty() == true) {
                return null;
            }

            //return strData;
            byte[] raw = aes_password;
            if (aes_password == null) {
                Date d = new Date();
               // System.out.println(d + ">>" + "Protect Data aes_password is NULL ");
                return null;
            } else {
//                System.out.println("Protect Data aes_password is NOT NULL with legth " + aes_password.length);
            }
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.encryptAES(aeskey1, strData.getBytes());

            String strEncryptedData = HWSignature.asHex(original);
            return strEncryptedData;
            //return new String(original);

            //byte[] raw = HWSignature.strHDDSignature.getBytes();
//            byte[] raw = aes_password;
//            raw = Arrays.copyOf(aes_password, 16);
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
//            byte[] encrypted = cipher.doFinal(strData.getBytes());
//            String strEncryptedData = HWSignature.asHex(encrypted);
//            return strEncryptedData;
            //return strData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //return strData;
        }
    }

    public static byte[] ProtectDataBytes(byte[] bytesData) {
        try {

            if (bytesData == null) {
                return null;
            }

            //System.out.println("ProtectDataBytes data size is " + bytesData.length);
            //return bytesData;
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.encryptAES(aeskey1, bytesData);
            return original;

//            byte[] raw = aes_password;
//            raw = Arrays.copyOf(aes_password, 16);
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance(AES_ALGO);
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
//            byte[] encrypted = cipher.doFinal(bytesData);
//            return encrypted;
            //return bytesData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //return bytesData;
        }
    }

    private static String GenerateRandomNumber() {
        Date d = new Date();
        return "" + d.getTime();
    }

    private static Properties g_LicenseProperties = null;

    public static int CheckEnforcementFor(String strType) {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                ex.printStackTrace();
                return -6;
            }
        }

        String status = g_LicenseProperties.getProperty(strType);
        if (status == null) {
            return -1;
        }

        if (status.compareToIgnoreCase("yes") == 0) {
            return 0;
        }

        return -1;
    }

    public static int GetChannelsAllowed() {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(CHANNELS);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
    }

    public static int GetOperatorsAllowed() {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(OPERATORS);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
    }
    
    public static int GetCountsAllowed(String property) {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(OPERATORS);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
    }

    public static int GetCallerCountAllowed() {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(THIRD_PARTY_CALLERS_COUNT);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
    }

    // New Users and Token License Code here
    public static int GetUsersAllowed() {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        try {
            String count = g_LicenseProperties.getProperty(USERS_COUNT);
        //    System.out.println("the count from license file for user is " + count);
            if (count == null) {
                return -1;
            }

            int iCount = Integer.parseInt(count);
            return iCount;
        } catch (Exception ex) {
            return -7;
        }

    }

    public static int GetOOBTokensAllowed() {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(OOBTOKEN_COUNT);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;

        //-998 is unlimited 
    }

    public static int GetSoftwareTokensAllowed() {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(SOFTWARETOKEN_COUNT);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
    }

    public static int GetHardwareTokensAllowed() {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(HARDWARETOKEN_COUNT);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
    }
    
     public static int GetSSOCountsAllowed(String property) {

        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(sso_applications_count);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
    }
     
     public static int GetAllowedIPsforCertDiscovery(String property)
     {
         
          if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(CERT_DISCOVERY_IPCOUNT);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
     
     
     }
     
     public static int GetAllowedSSLCertifcateCount(String property)
     {
         
          if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (LicenseFileSecurityException ex) {
                return -2;
            } catch (LicenseSecurityException ex) {
                return -3;
            } catch (LicenseHardwareIDException ex) {
                return -4;
            } catch (LicenseFileReadException ex) {
                return -5;
            } catch (LicenseContentException ex) {
                return -6;
            }
        }

        String count = g_LicenseProperties.getProperty(SSL_CERTIFICATE_COUNT);
        if (count == null) {
            return -1;
        }

        int iCount = Integer.parseInt(count);
        return iCount;
     
     
     }
     
    public static boolean GetAllowedFeature(String property) {
        if (g_LicenseProperties == null) {
            try {
                LicenseFile license = LicenseValidator.validateLicenseFile(
                        licfile,
                        publickey,
                        null,
                        null,
                        null,
                        null,
                        null);

                g_LicenseProperties = license.getAllSignedCustomFeatures();
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }

        String value = g_LicenseProperties.getProperty(property);
        if (value.equals("true")) {
            return true;
        } else {
            return false;
        }

    }
     
     
     
public static final String sso_applications_count = "sso.applications.count";
public static final String sso_enabled = "sso.enabled";

}
