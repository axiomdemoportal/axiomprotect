package com.mollatech.axiom.nucleus.crypto;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class TestOCRA {

    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }
        return strbuf.toString();
    }

    private static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        try {
            
//            String x="Hello I am Pramod";
//            byte[] d=x.getBytes();
//            for(int i=0;i<d.length;i++)
//            {
//                System.out.println("D"+d[]);
//            }



      
         String loginKey2= getHmacSHA1("pramod".getBytes(),"pramod\0".getBytes(), "HmacSHA1");
          System.out.println("JJJ"+loginKey2);//53b3a8413cf49e939d8711a0ba34916b2ec2db75
        //String loginKey2= getHmacSHA1("123456", "admin", "HmacSHA1");
     //   System.out.println(loginKey2);//3c39afa93e0b12c28f1f08b18488ebd4ad2e5858
        //String _sessionId = "npEFpAmrOVh oiBeMp9KCI/2 YNsg=";
            
           String _sessionId =  "r7wMJhvGuaD\n" +
"2U7Dax75rrkqn\n" +
"RW0=";

        _sessionId = _sessionId.trim();
        _sessionId = _sessionId.replaceAll(" ", "");
        _sessionId = _sessionId.replaceAll("\n", "");

            
//            MessageDigest md = MessageDigest.getInstance("SHA1");
//            md.update("test1234".getBytes());
//            byte[] output = md.digest();
//            //byte[] half_output = new byte[20];
//            //System.arraycopy(output, 0, half_output, 0, 16);
//            String strHexSecret = bytesToHex(output);
//            System.out.println(strHexSecret);
//            System.out.println("***********");
//
//            md = MessageDigest.getInstance("SHA-256");
//            md.update("test1234".getBytes());
//            output = md.digest();
//            //byte[] half_output = new byte[20];
//            //System.arraycopy(output, 0, h   alf_output, 0, 16);
//            strHexSecret = bytesToHex(output);
//            System.out.println(strHexSecret);
//            System.out.println("***********");

            
  

            
            String ocra = "";
            String seed = "";
            String ocraSuite = "";
            String counter = "";
            String password = "";
            String sessionInformation = "";
            String question = "";
            String qHex = "";
            String timeStamp = "";
            String PASS1234 = "f43a26706e5fd188e07b49a85882c189929dc81c";
            String SEED = "7f8ddadbaef4e9ee5f775f3c7b4edbe3d6bce7cf36735f3df76f5d73cd5c";
            String SEED32 = "31323334353637383930313233343536373839" + "30313233343536373839303132";
            String SEED64 = "31323334353637383930313233343536373839" + "3031323334353637383930313233343536373839" + "3031323334353637383930313233343536373839" + "3031323334";
            int STOP = 5;
            Date myDate = Calendar.getInstance().getTime();
            BigInteger b = new BigInteger("0");
            String sDate = "Mar 25 2008, 12:06:30 GMT";
            try {
                DateFormat df = new SimpleDateFormat("MMM dd yyyy, HH:mm:ss zzz");
                myDate = df.parse(sDate);
                b = new BigInteger("0" + myDate.getTime());
                b = b.divide(new BigInteger("60000"));
//                System.out.println("Time of \"" + sDate + "\" is in");
//                System.out.println("milli sec: " + myDate.getTime());
//                System.out.println("minutes: " + b.toString());
//                System.out.println("minutes (HEX encoded): " + b.toString(16).toUpperCase());
//                System.out.println("Time of \"" + sDate + "\" is the same as this localized");
//                System.out.println("time, \"" + new Date(myDate.getTime()) + "\"");
//                System.out.println();
//                System.out.println("Standard 20Byte key: " + "3132333435363738393031323334353637383930");
//                System.out.println("Standard 32Byte key: " + "3132333435363738393031323334353637383930");
//                System.out.println("            " + "313233343536373839303132");
//                System.out.println("Standard 64Byte key: 313233343536373839" + "3031323334353637383930");
//                System.out.println("                     313233343536373839" + "3031323334353637383930");
//                System.out.println("                     313233343536373839" + "3031323334353637383930");
//                System.out.println("                     31323334");
//                System.out.println();
                String pl="ﾌ";
                System.out.println("NeSybmol"+pl);
                //totp
               String ocraSuiteTOTP = "OCRA-1:TOTP-SHA1-6:QA64-T1M";
                timeStamp = b.toString(16);
                //for current DateTime
                Date dNow = new Date();
                BigInteger biNow = new BigInteger("0" + dNow.getTime());
                String strInt = new String("");
                strInt += "60" + "000";
                biNow = biNow.divide(new BigInteger(strInt));
                System.out.println("biNow"+biNow);
                String timeStamp1 = biNow.toString(16);
                System.out.println("TimeStamp ::   ::  :: " + timeStamp1);
            //    System.out.println("timeStamp1"+timeStamp1);
                  for (int i = 0; i < 10; i++) {
                    question = "" + i + i + i + i + i + i + i + i;
                    qHex = new String((new BigInteger(question, 10)).toString(16)).toUpperCase();
                    OCRA ocraObj = new OCRA();
                    ocra = ocraObj.generateOCRA(ocraSuiteTOTP, SEED32,"", "313233343536373839","", "",timeStamp1);
     System.out.println("Pramod: "+"ocraSuite"+ocraSuite +"Seed"+seed+"question"+ question + "  OCRA: " + ocra+"qHexPramod"+qHex+"TimeStampt"+timeStamp);
                    System.out.println("TOTP OCRA"+ocra);
                }
                  
                
                //end totp
                
//                System.out.println("Plain challenge response");
//                System.out.println("========================");
//                System.out.println();
//                ocraSuite = "OCRA-1:HOTP-SHA1-6:QN08";
//                System.out.println(ocraSuite);
//                System.out.println("=======================");
//                seed = SEED;
//                counter = "";
//                question = "";
//                password = "";
//                sessionInformation = "";
////                timeStamp = "";
//                for (int i = 0; i < 10; i++) {
//                    question = "" + i + i + i + i + i + i + i + i;
//                    qHex = new String((new BigInteger(question, 10)).toString(16)).toUpperCase();
//                    OCRA ocraObj = new OCRA();
//                    ocra = ocraObj.generateOCRA(ocraSuite, SEED32,"", "313233343536373839","", "", timeStamp);
//     System.out.println("Pramod: "+"ocraSuite"+ocraSuite +"Seed"+seed+"question"+ question + "  OCRA: " + ocra+"qHexPramod"+qHex+"TimeStampt"+timeStamp);
//                    System.out.println("Ocra OTP"+ocra);
//                }
//                System.out.println();
//                ocraSuite = "OCRA-1:HOTP-SHA256-8:C-QN08-PSHA1";
//                System.out.println(ocraSuite);
//                System.out.println("=================================");
//                seed = SEED32;
//                counter = "";
//                question = "12345678";
//                password = PASS1234;
//                sessionInformation = "";
////                timeStamp = "";
//                for (int i = 0; i < 10; i++) {
//                    counter = "" + i;
//                    qHex = new String((new BigInteger(question, 10)).toString(16)).toUpperCase();
//                    OCRA ocraObj = new OCRA();
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("Key: Standard 32Byte  C: " + counter + "  Q: " + question + "  PIN(1234): ");
//                    System.out.println(password + "  OCRA: " + ocra);
//                }
//                System.out.println();
//                ocraSuite = "OCRA-1:HOTP-SHA256-8:QN08-PSHA1";
//                System.out.println(ocraSuite);
//                System.out.println("===============================");
//                seed = SEED32;
//                counter = "";
//                question = "";
//                password = PASS1234;
//                sessionInformation = "";
//                timeStamp = "";
//                for (int i = 0; i < STOP; i++) {
//                    question = "" + i + i + i + i + i + i + i + i;
//                    qHex = new String((new BigInteger(question, 10)).toString(16)).toUpperCase();
//                    OCRA ocraObj = new OCRA();
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("Key: Standard 32Byte  Q: " + question + "  PIN(1234): ");
//                    System.out.println(password + "  OCRA: " + ocra);
//                }
//                System.out.println();
//                ocraSuite = "OCRA-1:HOTP-SHA512-8:C-QN08";
//                System.out.println(ocraSuite);
//                System.out.println("===========================");
//                seed = SEED64;
//                counter = "";
//                question = "";
//                password = "";
//                sessionInformation = "";
//                timeStamp = "";
//                for (int i = 0; i < 10; i++) {
//                    question = "" + i + i + i + i + i + i + i + i;
//                    qHex = new String((new BigInteger(question, 10)).toString(16)).toUpperCase();
//                    counter = "0000" + i;
//                    OCRA ocraObj = new OCRA();
//                    System.out.println("Before genearting otp"+"Counter"+counter+"qHex"+qHex+"Password"+password+"sessionInformation"+sessionInformation+"timeStamp"+timeStamp);
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("Key: Standard 64Byte  C: " + counter + "  Q: " + question + "  OCRA: " + ocra);
//                }
//                System.out.println();
//                ocraSuite = "OCRA-1:HOTP-SHA512-8:QN08-T1M";
//                System.out.println(ocraSuite);
//                System.out.println("=============================");
//                seed = SEED64;
//                counter = "";
//                question = "";
//                password = "";
//                sessionInformation = "";
//                timeStamp = b.toString(16);
//                for (int i = 0; i < STOP; i++) {
//                    question = "" + i + i + i + i + i + i + i + i;
//                    counter = "";
//                    qHex = new String((new BigInteger(question, 10)).toString(16)).toUpperCase();
//                    OCRA ocraObj = new OCRA();
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("Key: Standard 64Byte  Q: " + question + "  T: " + timeStamp.toUpperCase() + "  OCRA: " + ocra);
//                }
//                System.out.println();
//                System.out.println();
//                System.out.println("Mutual Challenge Response");
//                System.out.println("=========================");
//                System.out.println();
//                ocraSuite = "OCRA-1:HOTP-SHA256-8:QA08";
//                System.out.println("OCRASuite (server computation) = " + ocraSuite);
//                System.out.println("OCRASuite (client computation) = " + ocraSuite);
//                System.out.println("===============================" + "===========================");
//                seed = SEED32;
//                counter = "";
//                question = "";
//                password = "";
//                sessionInformation = "";
//                timeStamp = "";
//                for (int i = 0; i < STOP; i++) {
//                    question = "CLI2222" + i + "SRV1111" + i;
//                    qHex = asHex(question.getBytes());
//                    OCRA ocraObj = new OCRA();
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("(server)Key: Standard 32Byte  Q: " + question + "  OCRA: " + ocra);
//                    question = "SRV1111" + i + "CLI2222" + i;
//                    qHex = asHex(question.getBytes());
//                    OCRA ocraObj1 = new OCRA();
//                    ocra = ocraObj1.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("(client)Key: Standard 32Byte  Q: " + question + "  OCRA: " + ocra);
//                }
//                System.out.println();
//                String ocraSuite1 = "OCRA-1:HOTP-SHA512-8:QA08";
//                String ocraSuite2 = "OCRA-1:HOTP-SHA512-8:QA08-PSHA1";
//                System.out.println("OCRASuite (server computation) = " + ocraSuite1);
//                System.out.println("OCRASuite (client computation) = " + ocraSuite2);
//                System.out.println("===============================" + "=================================");
//                ocraSuite = "";
//                seed = SEED64;
//                counter = "";
//                question = "";
//                password = "";
//                sessionInformation = "";
//                timeStamp = "";
//                for (int i = 0; i < STOP; i++) {
//                    ocraSuite = ocraSuite1;
//                    question = "CLI2222" + i + "SRV1111" + i;
//                    qHex = asHex(question.getBytes());
//                    password = "";
//                    OCRA ocraObj = new OCRA();
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("(server)Key: Standard 64Byte  Q: " + question + "  OCRA: " + ocra);
//                    ocraSuite = ocraSuite2;
//                    question = "SRV1111" + i + "CLI2222" + i;
//                    qHex = asHex(question.getBytes());
//                    password = PASS1234;
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("(client)Key: Standard 64Byte  Q: " + question);
//                    System.out.println("P: " + password.toUpperCase() + "  OCRA: " + ocra);
//                }
//                System.out.println();
//                System.out.println();
//                System.out.println("Plain Signature");
//                System.out.println("===============");
//                System.out.println();
//                ocraSuite = "OCRA-1:HOTP-SHA256-8:QA08";
//                System.out.println(ocraSuite);
//                System.out.println("=========================");
//                seed = SEED32;
//                counter = "";
//                question = "";
//                password = "";
//                sessionInformation = "";
//                timeStamp = "";
//                for (int i = 0; i < STOP; i++) {
//                    question = "SIG1" + i + "000";
//                    qHex = asHex(question.getBytes());
//                    OCRA ocraObj = new OCRA();
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("Key: Standard 32Byte  Q(Signature challenge): " + question);
//                    System.out.println("   OCRA: " + ocra);
//                }
//                System.out.println();
//                ocraSuite = "OCRA-1:HOTP-SHA512-8:QA10-T1M";
//                System.out.println(ocraSuite);
//                System.out.println("=============================");
//                seed = SEED64;
//                counter = "";
//                question = "";
//                password = "";
//                sessionInformation = "";
//                timeStamp = b.toString(16);
//                for (int i = 0; i < STOP; i++) {
//                    question = "SIG1" + i + "00000";
//                    qHex = asHex(question.getBytes());
//                    OCRA ocraObj = new OCRA();
//                    ocra = ocraObj.generateOCRA(ocraSuite, seed, counter, qHex, password, sessionInformation, timeStamp);
//                    System.out.println("Key: Standard 64Byte  Q(Signature challenge): " + question);
//                    System.out.println("   T: " + timeStamp.toUpperCase() + "  OCRA: " + ocra);
//                }
            } catch (Exception e) {
               // System.out.println("Error : " + e);
            }

        } catch (Exception ex) {
            Logger.getLogger(TestOCRA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
     public static String getHmacSHA1(byte[] password,byte[] loginname, String algorithm) throws InvalidKeyException{
       // byte[] keyBytes = password.getBytes();
        Key key = new SecretKeySpec(password, 0, password.length, algorithm);
        Mac mac=null;
        try {
            mac = Mac.getInstance(algorithm);
            mac.init(key);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return byteArrayToHex(mac.doFinal(loginname));
    }
    
     //OCRA-1:TOTP-SHA1-6:QA64-T1M 123456789                                                                                                                           w�
    
    /**
     * 16进制加密
     * @param a
     * @return
     */
    protected static String byteArrayToHex(byte [] a) {
        int hn, ln, cx;
        String hexDigitChars = "0123456789abcdef";
        StringBuffer buf = new StringBuffer(a.length * 2);
        for(cx = 0; cx < a.length; cx++) {
            hn = ((int)(a[cx]) & 0x00ff) /16 ;
            ln = ((int)(a[cx]) & 0x000f);
            buf.append(hexDigitChars.charAt(hn));
            buf.append(hexDigitChars.charAt(ln));
        }
        return buf.toString();

    }
}


