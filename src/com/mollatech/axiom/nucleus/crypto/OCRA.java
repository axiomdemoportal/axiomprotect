package com.mollatech.axiom.nucleus.crypto;

import java.io.UnsupportedEncodingException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.Key;
import org.bouncycastle.util.encoders.Base64;

public class OCRA {

    public OCRA() {

    }

    /**
     * This method uses the JCE to provide the crypto algorithm. HMAC computes a
     * Hashed Message Authentication Code with the crypto hash algorithm as a
     * parameter.
     *
     * @param crypto the crypto algorithm (HmacSHA1, HmacSHA256, HmacSHA512)
     * @param keyBytes the bytes to use for the HMAC key
     * @param text the message or text to be authenticated.
     */
    private byte[] hmac_sha1(String crypto,
            byte[] keyBytes, byte[] text) {
        Mac hmac = null;
        try {
            hmac = Mac.getInstance(crypto);
            SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
            hmac.init(macKey);
            return hmac.doFinal(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] hmac_sha1ForWeb(String crypto,
            byte[] keyBytes, byte[] text) {
        Mac hmac = null;
        try {
            hmac = Mac.getInstance(crypto);
            Key key = new SecretKeySpec(keyBytes, 0, keyBytes.length, crypto);
            // Key macKey = new SecretKeySpec(keyBytes,keyBytes.length, "RAW");
            hmac.init(key);
            return hmac.doFinal(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static final int[] DIGITS_POWER // 0 1  2   3    4     5      6       7        8
            = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};

    /**
     * This method converts HEX string to Byte[]
     *
     * @param hex the HEX string
     *
     * @return A byte array
     */
    private byte[] hexStr2Bytes(String hex) {
        // Adding one byte to get the right conversion
        // values starting with "0" can be converted
        byte[] bArray = new BigInteger("10" + hex, 16).toByteArray();

        // Copy all the REAL bytes, not the "first"
        byte[] ret = new byte[bArray.length - 1];
        System.arraycopy(bArray, 1, ret, 0, ret.length);
        return ret;
    }

    /**
     * This method generates an OCRA HOTP value for the given set of parameters.
     *
     * @param ocraSuite the OCRA Suite
     * @param key the shared secret, HEX encoded
     * @param counter the counter that changes on a per use basis, HEX encoded
     * @param question the challenge question, HEX encoded
     * @param password a password that can be used, HEX encoded
     * @param sessionInformation Static information that identifies the current
     * session, Hex encoded
     * @param timeStamp a value that reflects a time
     *
     * @return A numeric String in base 10 that includes
     * {@link truncationDigits} digiHOTPts
     *
     *
     *
     */
    public String generateOCRA(String ocraSuite,
            String key,
            String counter,
            String question,
            String password,
            String sessionInformation,
            String timeStamp){
      
        int codeDigits = 0;
        String crypto = "";
        String result = null;
        int ocraSuiteLength = (ocraSuite.getBytes()).length;
        int counterLength = 0;
        int questionLength = 0;
        int passwordLength = 0;
        int sessionInformationLength = 0;
        int timeStampLength = 0;

        // The OCRASuites components
        String CryptoFunction = ocraSuite.split(":")[1];
        String DataInput = ocraSuite.split(":")[2];

        if (CryptoFunction.toLowerCase().indexOf("sha1") > 1) {
            crypto = "HmacSHA1";
        }
        if (CryptoFunction.toLowerCase().indexOf("sha256") > 1) {
            crypto = "HmacSHA256";
        }
        if (CryptoFunction.toLowerCase().indexOf("sha512") > 1) {
            crypto = "HmacSHA512";
        }
       // System.out.println("Crtypto"+crypto);
        // How many digits should we return
        codeDigits = Integer.decode(CryptoFunction.substring(
                CryptoFunction.lastIndexOf("-") + 1));
      //  System.out.println("CodeSigits"+codeDigits);
        // The size of the byte array message to be encrypted
        // Counter
        if (DataInput.toLowerCase().startsWith("c")) {
            // Fix the length of the HEX string
            while (counter.length() < 16) {
                counter = "0" + counter;
            }
            counterLength = 8;
        }
        // Question - always 128 bytes
        if (DataInput.toLowerCase().startsWith("q")
                || (DataInput.toLowerCase().indexOf("-q") >= 0)) {
            while (question.length() < 256) {
                question = question + "0";
            }

            questionLength = 128;
        }

        //System.out.println(question);
        //System.out.println(question.length());
        // Password - sha1
        if (DataInput.toLowerCase().indexOf("psha1") > 1) {
            while (password.length() < 40) {
                password = "0" + password;
            }
            passwordLength = 20;
        }

        // Password - sha256
        if (DataInput.toLowerCase().indexOf("psha256") > 1) {
            while (password.length() < 64) {
                password = "0" + password;
            }
            passwordLength = 32;
        }

        // Password - sha512
        if (DataInput.toLowerCase().indexOf("psha512") > 1) {
            while (password.length() < 128) {
                password = "0" + password;
            }
            passwordLength = 64;
        }

        // sessionInformation - s064
        if (DataInput.toLowerCase().indexOf("s064") > 1) {
            while (sessionInformation.length() < 128) {
                sessionInformation = "0" + sessionInformation;
            }
            sessionInformationLength = 64;
        }

        // sessionInformation - s128
        if (DataInput.toLowerCase().indexOf("s128") > 1) {
            while (sessionInformation.length() < 256) {
                sessionInformation = "0" + sessionInformation;
            }
            sessionInformationLength = 128;
        }

        // sessionInformation - s256
        if (DataInput.toLowerCase().indexOf("s256") > 1) {
            while (sessionInformation.length() < 512) {
                sessionInformation = "0" + sessionInformation;
            }
            sessionInformationLength = 256;
        }

        // sessionInformation - s512
        if (DataInput.toLowerCase().indexOf("s512") > 1) {
            while (sessionInformation.length() < 1024) {
                sessionInformation = "0" + sessionInformation;
            }
            sessionInformationLength = 512;
        }

        // TimeStamp
        if (DataInput.toLowerCase().startsWith("t")
                || (DataInput.toLowerCase().indexOf("-t") > 1)) {
            while (timeStamp.length() < 16) {
                timeStamp = "0" + timeStamp;
            }
            timeStampLength = 8;
        }

//        System.out.println("ocraSuiteLength" + ocraSuiteLength);
//        System.out.println("counterLength" + counterLength);
//        System.out.println("questionLength" + questionLength);
//        System.out.println("passwordLength" + passwordLength);
//        System.out.println("sessionInformationLength" + sessionInformationLength);
//        System.out.println("timeStampLength" + timeStampLength);
        byte[] msg = new byte[ocraSuiteLength
                + counterLength
                + questionLength
                + passwordLength
                + sessionInformationLength
                + timeStampLength
                + 1];
        byte[] bArray = ocraSuite.getBytes();
        
        System.arraycopy(bArray, 0, msg, 0, bArray.length);
        
        msg[bArray.length] = 0x00;
        
       
        
       // System.out.println("msglength:" + msg.length);
        if (counterLength > 0) {
            bArray = hexStr2Bytes(counter);
            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1,
                    bArray.length);
        }
        if (questionLength > 0) {
            bArray = hexStr2Bytes(question);
            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1
                    + counterLength, bArray.length);
        }
        if (passwordLength > 0) {
            bArray = hexStr2Bytes(password);
            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1
                    + counterLength + questionLength, bArray.length);

        }
        if (sessionInformationLength > 0) {
            bArray = hexStr2Bytes(sessionInformation);
            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1
                    + counterLength + questionLength
                    + passwordLength, bArray.length);
        }
      //  System.out.println("MSGBeforeTimeStamp" + new String(msg));

        if (timeStampLength > 0) {
            bArray = hexStr2Bytes(timeStamp);
           // System.out.println("HexofTimeStamp" + new String(bArray));
//            for (int i = 0; i < bArray.length; i++) {
//                System.out.println("B" + bArray[i]);
//            }

            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1
                    + counterLength + questionLength
                    + passwordLength + sessionInformationLength,
                    bArray.length);
        }
//        for(int i=0;i<msg.length;i++)
//        {
//            System.out.println("MSGBIT"+msg[i]);
//        }
       
         OCRA ocra= new OCRA();
       
      //  System.out.println("PPP" + ppp);
        bArray = hexStr2Bytes(key);
      //byte[] hash = hmac_sha1(crypto,bArray,ppp.getBytes());
      
        byte[] hash = hmac_sha1(crypto,bArray,msg);
        System.out.println("hash" + hash.length);
        int offset = hash[hash.length - 1] & 0xf;
        int binary
                = ((hash[offset] & 0x7f) << 24)
                | ((hash[offset + 1] & 0xff) << 16)
                | ((hash[offset + 2] & 0xff) << 8)
                | (hash[offset + 3] & 0xff);

        int otp = binary % DIGITS_POWER[codeDigits];

        result = Integer.toString(otp);
        System.out.println("OTP"+otp);
        while (result.length() < codeDigits) {
            result = "0" + result;
        }
        return result;
    }
    
    
    public String generateOCRAWeb(String ocraSuite,
            String key,
            String counter,
            String question,
            String password,
            String sessionInformation,
            String timeStamp){
      
        int codeDigits = 0;
        String crypto = "";
        String result = null;
        int ocraSuiteLength = (ocraSuite.getBytes()).length;
        int counterLength = 0;
        int questionLength = 0;
        int passwordLength = 0;
        int sessionInformationLength = 0;
        int timeStampLength = 0;

        // The OCRASuites components
        String CryptoFunction = ocraSuite.split(":")[1];
        String DataInput = ocraSuite.split(":")[2];

        if (CryptoFunction.toLowerCase().indexOf("sha1") > 1) {
            crypto = "HmacSHA1";
        }
        if (CryptoFunction.toLowerCase().indexOf("sha256") > 1) {
            crypto = "HmacSHA256";
        }
        if (CryptoFunction.toLowerCase().indexOf("sha512") > 1) {
            crypto = "HmacSHA512";
        }

        // How many digits should we return
        codeDigits = Integer.decode(CryptoFunction.substring(
                CryptoFunction.lastIndexOf("-") + 1));

        // The size of the byte array message to be encrypted
        // Counter
        if (DataInput.toLowerCase().startsWith("c")) {
            // Fix the length of the HEX string
            while (counter.length() < 16) {
                counter = "0" + counter;
            }
            counterLength = 8;
        }
        // Question - always 128 bytes
        if (DataInput.toLowerCase().startsWith("q")
                || (DataInput.toLowerCase().indexOf("-q") >= 0)) {
            while (question.length() < 256) {
                question = question + "0";
            }

            questionLength = 128;
        }

        //System.out.println(question);
        //System.out.println(question.length());
        // Password - sha1
        if (DataInput.toLowerCase().indexOf("psha1") > 1) {
            while (password.length() < 40) {
                password = "0" + password;
            }
            passwordLength = 20;
        }

        // Password - sha256
        if (DataInput.toLowerCase().indexOf("psha256") > 1) {
            while (password.length() < 64) {
                password = "0" + password;
            }
            passwordLength = 32;
        }

        // Password - sha512
        if (DataInput.toLowerCase().indexOf("psha512") > 1) {
            while (password.length() < 128) {
                password = "0" + password;
            }
            passwordLength = 64;
        }

        // sessionInformation - s064
        if (DataInput.toLowerCase().indexOf("s064") > 1) {
            while (sessionInformation.length() < 128) {
                sessionInformation = "0" + sessionInformation;
            }
            sessionInformationLength = 64;
        }

        // sessionInformation - s128
        if (DataInput.toLowerCase().indexOf("s128") > 1) {
            while (sessionInformation.length() < 256) {
                sessionInformation = "0" + sessionInformation;
            }
            sessionInformationLength = 128;
        }

        // sessionInformation - s256
        if (DataInput.toLowerCase().indexOf("s256") > 1) {
            while (sessionInformation.length() < 512) {
                sessionInformation = "0" + sessionInformation;
            }
            sessionInformationLength = 256;
        }

        // sessionInformation - s512
        if (DataInput.toLowerCase().indexOf("s512") > 1) {
            while (sessionInformation.length() < 1024) {
                sessionInformation = "0" + sessionInformation;
            }
            sessionInformationLength = 512;
        }

        // TimeStamp
        if (DataInput.toLowerCase().startsWith("t")
                || (DataInput.toLowerCase().indexOf("-t") > 1)) {
            while (timeStamp.length() < 16) {
                timeStamp = "0" + timeStamp;
            }
            timeStampLength = 8;
        }

       // System.out.println("timeStampLength" + timeStampLength);
        byte[] msg = new byte[ocraSuiteLength
                + counterLength
                + questionLength
                + passwordLength
                + sessionInformationLength
                + timeStampLength
                + 1];
        byte[] bArray = ocraSuite.getBytes();
        System.arraycopy(bArray, 0, msg, 0, bArray.length);
        msg[bArray.length] = 0x00;

        //System.out.println("msglength:" + msg.length);
        if (counterLength > 0) {
            bArray = hexStr2Bytes(counter);
            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1,
                    bArray.length);
        }
        if (questionLength > 0) {
            bArray = hexStr2Bytes(question);
            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1
                    + counterLength, bArray.length);
        }
        if (passwordLength > 0) {
            bArray = hexStr2Bytes(password);
            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1
                    + counterLength + questionLength, bArray.length);

        }
        if (sessionInformationLength > 0) {
            bArray = hexStr2Bytes(sessionInformation);
            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1
                    + counterLength + questionLength
                    + passwordLength, bArray.length);
        }
        //System.out.println("MSGBeforeTimeStamp" + new String(msg));

        if (timeStampLength > 0) {
            bArray = hexStr2Bytes(timeStamp);
            //System.out.println("HexofTimeStamp" + new String(bArray));
            for (int i = 0; i < bArray.length; i++) {
               // System.out.println("B" + bArray[i]);
            }

            System.arraycopy(bArray, 0, msg, ocraSuiteLength + 1
                    + counterLength + questionLength
                    + passwordLength + sessionInformationLength,
                    bArray.length);
        }
//        for(int i=0;i<msg.length;i++)
//        {
//            System.out.println("MSGBIT"+msg[i]);
//        }
        String ppp = new String(msg);
      //  System.out.println("PPP" + ppp);
        bArray = hexStr2Bytes(key);
          byte[] x=Base64.encode(msg);
        String zxx=  new String(x);
      // byte[] hash = hmac_sha1(crypto,bArray,ppp.getBytes());
        byte[] hash = hmac_sha1(crypto,bArray,zxx.getBytes());
       // System.out.println("Hash"+ asHex(hash));
        int offset = hash[hash.length - 1] & 0xf;
        int binary
                = ((hash[offset] & 0x7f) << 24)
                | ((hash[offset + 1] & 0xff) << 16)
                | ((hash[offset + 2] & 0xff) << 8)
                | (hash[offset + 3] & 0xff);

        int otp = binary % DIGITS_POWER[codeDigits];

        result = Integer.toString(otp);
        while (result.length() < codeDigits) {
            result = "0" + result;
        }
        return result;
    }
    
     public  String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }
        return strbuf.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
