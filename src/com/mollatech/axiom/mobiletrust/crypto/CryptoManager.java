/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.mobiletrust.crypto;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.OCRA;
import com.mollatech.axiom.nucleus.crypto.TOTP;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import org.json.JSONObject;

/**
 *
 * @author vikramsareen
 */
public class CryptoManager {
public static int ACTIVATE = 1;
public static int GENERAL_PROCESS = 2;
//    public byte[] encryptRSATempECB(byte[] plainData, PublicKey pubKey) {
//        try {
//            byte[] encryptionByte = null;
//            //Cipher cipher = Cipher.getInstance("RSA");
//            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
//            //encryptionByte = cipher.doFinal(plainData);
//            byte[] someData = cipher.update(plainData);
//            byte[] moreData = cipher.doFinal();
//            byte[] encrypted = new byte[someData.length + moreData.length];
//            System.arraycopy(someData, 0, encrypted, 0, someData.length);
//            System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);
//
//            encryptionByte = encrypted;
//            return encryptionByte;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    private byte[] encryptRSATempNONE(byte[] plainData, PublicKey pubKey) {
//        try {
//            byte[] encryptionByte = null;
//            Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
//            encryptionByte = cipher.doFinal(plainData);
//            return encryptionByte;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    private byte[] decryptRSATempECB(byte[] encrptdByte, PrivateKey privateKey) {
//        try {
//            byte[] encryptionByte = null;
//            //Cipher cipher = Cipher.getInstance("RSA");
//            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//            cipher.init(Cipher.DECRYPT_MODE, privateKey);
//            //encryptionByte = cipher.doFinal(encrptdByte);
//            //System.out.println("Recovered String     :::  " + new String(encryptionByte));
//            byte[] someDecrypted = cipher.update(encrptdByte);
//            byte[] moreDecrypted = cipher.doFinal();
//            byte[] decrypted = new byte[someDecrypted.length + moreDecrypted.length];
//            System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
//            System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);
//
//            encryptionByte = decrypted;
//
//            return encryptionByte;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    private byte[] decryptRSATempNONE(byte[] encrptdByte, PrivateKey privateKey) {
//        try {
//            byte[] encryptionByte = null;
//            Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");
//            cipher.init(Cipher.DECRYPT_MODE, privateKey);
//            encryptionByte = cipher.doFinal(encrptdByte);
//            //System.out.println("Recovered String     :::  " + new String(encryptionByte));
//            return encryptionByte;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
   static String AES_ALGO = "AES/CBC/PKCS5Padding";
    
    //changed for testing PUK
    
    
    
    //private static String AES_ALGO = "AES/CBC";

    //private static String AES_ALGO = "AES";
    final private static byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    public static final int RSA = 1;
    public static final int SHA1WithRSA = 2;
    public static final int ANDROID = 1;
    public static final int IOS = 2;
    public static final int webtrust=3;
//     static {
//     System.out.println("before adding BC");
//     Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
//     System.out.println("after adding BC");
// }

    private static byte[] v = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    static {
        Provider p = Security.getProvider("BC");
        if (p == null) {
            Security.addProvider(new BouncyCastleProvider()); // add it
        }
    }

    public static void main(String args[]) throws Exception {

//        CryptoManager cm = new CryptoManager();
//        AxiomRSAKeyPair akeypair = cm.generateAxiomRSAKeyPair(512);
//        PublicKey pubkey = akeypair.visiblekey;
//        PrivateKey prvkey = akeypair.privatekey;
//
//        PublicKey pubkey = cm.getPublicKey("MIIBCgKCAQEA0btU20f6jgJmJEqwef+1q16HUQJMl10Gk9uqJl7/S/qAZBsZ26Lgmp3Pdkcai3G8v8OhfZqn2J7Vl9lYxAWgJIBSbxknsdYNUIN65+x6DXZ0tznLQGlUn5yYcm6wMuP0P3K77DQfXzDg3YKlX58y6nDTBvw47iXFhuTevrjHULBhBbdN3Kes816cridq5NP+PNOoVEn/ahsf88MoT+vVZ99IlgGnSubpVlouDXm+dlr4DhPg70vGfbawOmSGLfbj6Uft/ndP3vB/xgFxsrRvVv5a063nebZC/qg7PCPUtVlUl71mHsve0PX0DVmvF5JqqSbZJTHo7ck4a+NBtmMUnwIDAQAB");
       String key = "MIIBCgKCAQEA0btU20f6jgJmJEqwef+1q16HUQJMl10Gk9uqJl7/S/qAZBsZ26Lgmp3Pdkcai3G8v8OhfZqn2J7Vl9lYxAWgJIBSbxknsdYNUIN65+x6DXZ0tznLQGlUn5yYcm6wMuP0P3K77DQfXzDg3YKlX58y6nDTBvw47iXFhuTevrjHULBhBbdN3Kes816cridq5NP+PNOoVEn/ahsf88MoT+vVZ99IlgGnSubpVlouDXm+dlr4DhPg70vGfbawOmSGLfbj6Uft/ndP3vB/xgFxsrRvVv5a063nebZC/qg7PCPUtVlUl71mHsve0PX0DVmvF5JqqSbZJTHo7ck4a+NBtmMUnwIDAQAB";
//        String pemKey = "-----BEGIN RSA PUBLIC KEY-----\n"
//                    + key + "\n"
//                    + "-----END RSA PUBLIC KEY-----\n";
//        String key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA139Xu0bHH6wipCj/tLnsd4pvug+GFFpnbDkl95hdyOFAsvlBFSB6NN04I7+8v/QLG/t+3+VqNMtCd60g6Ajos6yzdHrz9gKMhrggmWoeO0At3OKW70aO0yjOOjm5Qsr2Dl19dPvB8XhpdLmqmO+EkTb7GWQZtY7tIQocG+k/Mnss7/5X6HC9QouKGeWlMx+AVjcTFAlewvAEXHVkL5Rm+S3+1QVHnjj2X38uGMgKAPSmmi9/3pUlJi86i8Jv9tvQkT04JQf3B8CuRv9FQtWX9opf6tKrpX6a7gNRXKhu1xs3qItNwlYgW9OtAownGCwL+pARSQWzRp5U8rs33s1HDwIDAQAB";
        X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(key));
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey publicKey = fact.generatePublic(spec);
//        String data = "manoj";
//        byte[] encryptedDataUsingECB = cm.encryptRSATempECB(data.getBytes(), pubkey);
//        byte[] encryptedDataUsingNONE = cm.encryptRSATempNONE(data.getBytes(), pubkey);
//
//        System.out.println("Using ECB::" + new String(Base64.encode(encryptedDataUsingECB)));
//        System.out.println("Using NONE::" + new String(Base64.encode(encryptedDataUsingNONE)));
//
//        byte[] decryptedDataUsingECB = cm.decryptRSATempECB(encryptedDataUsingECB, prvkey);
//        byte[] decryptedDataUsingNONE = cm.decryptRSATempNONE(encryptedDataUsingNONE, prvkey);
//
//        System.out.println("Using ECB (decrypted)::" + new String(decryptedDataUsingECB));
//        System.out.println("Using NONE (decrypted)::" + new String(decryptedDataUsingNONE));
//
//        decryptedDataUsingNONE = null;
//        decryptedDataUsingECB = null;
//
//        decryptedDataUsingNONE = cm.decryptRSATempECB(encryptedDataUsingECB, prvkey);
//        decryptedDataUsingECB = cm.decryptRSATempNONE(encryptedDataUsingNONE, prvkey);
//
//        System.out.println("Using ECB (decrypted reversed)::" + new String(decryptedDataUsingECB));
//        System.out.println("Using NONE (decrypted reversed)::" + new String(decryptedDataUsingNONE));
        CryptoManager cmObj = new CryptoManager();
//        byte[] b = cmObj.generateKeyAESForAndroid("test password".toCharArray(), 128);
//        System.out.println("encryption key 1 is ::" + new String(Base64.encode(b)));
//
//        b = cmObj.generateKeyAESForAndroid("test password".toCharArray(), 128);
//        System.out.println("encrypted key 2 is ::" + new String(Base64.encode(b)));

//        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
//        
//        System.out.println("Secure Random without Crypto" + sr.generateSeed(10));
//        
//        SecureRandom sr1 =  SecureRandom.getInstance("SHA1PRNG", "Crypto");
//        
//        System.out.println("Secure Random with Crypto" + sr1.generateSeed(10));
//        String otptokenSecret = "BaEf9F1s8FGB5okFFjcdZsOQjdzPYqlls6geCGNWu0HXBhJkNlOeaG0tshl1oHDI";
//        CryptoManager c = new CryptoManager();
//        byte[] secByte = Base64.decode(otptokenSecret);
//        String filepath = "/Users/vikramsareen/Desktop/all/acleda/axiom protect/FPT/IdeaLink Seed Code/AES128 Format/20140320-IDEA001-1554.bin";
//        String strDecryptedSecret = c.decryptSecreteAES128(secByte, filepath);
//        System.out.println(strDecryptedSecret);
//        System.out.println(strDecryptedSecret);
//        String secrete = "URFIYzgCiBP1Ncxg345oLvMmDrDhbxGSbikFdlfQ7ACxNwguOJbbQJu1Jcd4B3nW";
//        byte[] secByte = Base64.decode(secrete);
//        String filepath = "D:\\_nk\\IdeaLink Seed Code\\IdeaLink Seed Code\\AES128 Format\\20140320-IDEA001-1554.bin";
//        c.decryptSecreteAES128(secByte, filepath);
        //String sec256 = "5/JISSXJdZLYBmXBrHT3Nseps6T8r9a2Sqtp86/j4F5rRY1FuSTh/qQ5T9JyEytD";
        //String file256 = "/Users/nileshkamble/Downloads/IdeaLinkSeedCode/AES256Format/20140320-IDEA001-1554.bin";
        //byte[] secByte256 = Base64.decode(sec256);
        //c.decryptSecreteAES256(secByte256, file256);
//         CheckAESEncrypt();
//        AES aesObj = new AES();
//      String port =   aesObj.PINEncrypt("axiomv2aliance", AES.getSignature());
//        System.out.println("Port     :   " + port);
    }

//    public static void main(String args[]) throws UnsupportedEncodingException {
//        CryptoManager cm = new CryptoManager();
////        PublicKey key = cm.getPublicKey("MIGJAoGBAMtuarPi14Z3YghXUDrB+c3qA83vaTxxKkzgBFYxYQXlqAylmZs5vylgSz9TZiiHTJHrN2BhDNE16Vu9HqVZa5kh3Cc/L7A434mJzzbXmYaPashZvptBfTXOw5/Kl+0Q/pS6YCQpqn0Fb+vDF1zjMtbk4b3BOBdfyd7t0xt6BJvLAgMBAAE=");
////        String data = "manoj";
////
////        byte[] da = cm.encryptRSA(data.getBytes(), key);
////        //System.out.println(new String(da));
////
////        byte[] s = Base64.encode(key.getEncoded());
////        String vKey = new String(s);
////        //System.out.println("Public Key   ::  " + vKey);
//
//        String mydata = new String("这是一条测试消息 this is sample");
//        
//        for(int i=0;i<mydata.length();i++) { 
//            char ch = mydata.charAt(i);
//            System.out.print(String.format("%04x", (int) ch));
//        }
//        
//        System.out.println();
//        
//        //System.out.println(cm.asHex(mydata.getBytes("UTF8")));
//        
//        String [] _numbers = "60103663542;610333333;605955959;".split(";");
//        //String [] _numbers = "60103663542".split(";");
//        for (int i = 0; i < _numbers.length; i++) {
//            System.out.println(_numbers[i]);
//        }
//        
//        
//        
//        //CryptoManager cm = new CryptoManager();
//        //cm.CheckAESEncrypt();
//    }
    public String generateTokenSecret(String regcode, String deviceid) {
        return generateTokenSecretInner(regcode, deviceid);
    }

    private String generateTokenSecretInner(String regcode, String deviceid) {

        try {
            String strRegCodeSecret = deviceid;
            strRegCodeSecret += regcode;
            Date d = new Date();
            strRegCodeSecret += d.getTime();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(strRegCodeSecret.getBytes());
            byte[] output = md.digest();
            String strHexSecret = asHex(output);
            return strHexSecret;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

    public String generateOTP(String secret, int iLength, int otpDurationinSeconds) {
        return generateOTPInner(secret, iLength, otpDurationinSeconds);
    }

    private String generateOTPInner(String secret, int iLength, int otpDurationinSeconds) {
        long tOTP = new java.util.Date().getTime() / 1000;
        String strOTPLength = "";
        strOTPLength += iLength;
        TOTP totpObj = new TOTP();
        String otp = totpObj.GetOTP(secret,
                tOTP,
                strOTPLength,
                0,
                1,
                otpDurationinSeconds);

        Date d = new Date(tOTP * 1000);
        return otp;
    }

    public String generateSignatureOTP(String secret, String[] data, int iOtpDurationInSeconds, int iSOTPLength) throws Exception {
        return generateSignatureOTPInner(secret, data, iOtpDurationInSeconds, iSOTPLength);
    }

    private String generateSignatureOTPInner(String secret, String[] data, int iOtpDurationInSeconds, int iSOTPLength) throws Exception {

        String strAllData = "";
        for (int i = 0; i < data.length; i++) {
            strAllData += data[i];
        }

        //SHA1 the data and then use it
        //OCRA-1:HOTP-SHA1-8:QN64-T1M
        String ocraSuite = "OCRA-1:TOTP-SHA1-";
        ocraSuite += iSOTPLength;
        ocraSuite += ":QN64";
        if (iOtpDurationInSeconds == 60) {
            ocraSuite += "-T1M";
        } else if (iOtpDurationInSeconds == 120) {
            ocraSuite += "-T2M";
        } else if (iOtpDurationInSeconds == 180) {
            ocraSuite += "-T3M";
        }

        String questionHex = asHex(strAllData.getBytes());
        BigInteger b = new BigInteger("0");
        java.util.Date date1 = new java.util.Date();
        b = BigInteger.valueOf(date1.getTime());
        long otptimeToCal = iOtpDurationInSeconds * 1000;
        b = b.divide(new BigInteger("" + otptimeToCal));
        String timeStamp = b.toString(16);

        OCRA ocraObj = new OCRA();

        String sotp = ocraObj.generateOCRA(
                ocraSuite,
                secret,
                "",
                questionHex,
                "",
                "",
                timeStamp);
        return sotp;
    }

    public AxiomRSAKeyPair generateAxiomRSAKeyPair(int iLength) throws NoSuchAlgorithmException {
        return generateAxiomRSAKeyPairInner(iLength);
    }

    public boolean verifyDataRSA(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        return verifyDataRSAInner(data, sigBytes, publicKey, type);
    }
    
    public boolean verifyDataRSAv2(byte[] data, byte[] sigBytes, PublicKey publicKey, int type,String hashallgo) {
        return verifyDataRSAInnerv2(data, sigBytes, publicKey, type,hashallgo);
    }
    
    
    public boolean verifyDataRSA1(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        return verifyDataRSAInner1(data, sigBytes, publicKey, type);
    }

    public byte[] signDataRSA(byte[] data, PrivateKey privateKey, int type) {
        return signDataRSAInner(data, privateKey, type);
    }
    
     public byte[] signDataRSAv2(byte[] data, PrivateKey privateKey, String hashAlgo,int devicetype)
     {
        return signDataRSAInnerSha266(data, privateKey, hashAlgo, devicetype);
     }
    
    
     public byte[] signDataRSAforWebtrust(byte[] data, PrivateKey privateKey, int type) {
        return signDataRSAInnerforWebtrust(data, privateKey, type);
    }

    public byte[] encryptRSA(byte[] plainData, PublicKey pubKey) {
        return encryptRSAInner(plainData, pubKey);
    }

    public byte[] encryptRSAForAndroid(byte[] plainData, PublicKey pubKey) {
        return encryptRSAForAndroidInner(plainData, pubKey);
    }

    public byte[] decryptRSA(byte[] encrptdByte, PrivateKey privateKey) {
        return decryptRSAInner(encrptdByte, privateKey);
    }

    public byte[] decryptRSAForAndroid(byte[] encrptdByte, PrivateKey privateKey) {
        return decryptRSAForAndroidInner(encrptdByte, privateKey);
    }

    public byte[] generateKeyAES(byte[] keyStart, int iLength) {
        return generateKeyAESInner(keyStart, iLength);
    }

    public byte[] generateKeyAESForAndroid(char[] keyStart, int iLength) {
        return generateKeyAESForAndroidInner(keyStart, iLength);
    }

    public byte[] encryptAES(byte[] key, byte[] clear) {
        //String strKey = new String(Base64.encode(key));
        //String data = new String(Base64.encode(clear));
        //System.out.println("encryptAES Key::" + strKey);
        //System.out.println("encryptAES Data::" + data);

        return encryptAESInner(key, clear);
        //return clear;
    }

    public byte[] decryptAES(byte[] key, byte[] encrypted) {
        //String strKey = new String(Base64.encode(key));
        //String data = new String(Base64.encode(encrypted));
        //System.out.println("Key     ::   " + strKey);
        //System.out.println("Data     ::   " + data);
        return decryptAESInner(key, encrypted);
        //return encrypted;
    }
    
    public byte[] decryptAesforWebTrust(String passphrase, String salt,String iv,String ciphertext) {
        byte[] plaintext = null;
        try {
            int iterationCount = 1000;//Integer.parseInt(request.getParameter("iterationCount"));
            int keySize = 128;//Integer.parseInt(request.getParameter("keySize"));
            AesUtil aesUtil = new AesUtil(keySize, iterationCount);
            String outPut = aesUtil.decrypt(salt, iv, passphrase, ciphertext);
            plaintext = Base64.decode(outPut);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return plaintext;

    }
    
     public byte[] encryptAesforWebTrust(String passphrase, String salt,String iv,String plaintext) {
        byte[] cphertext = null;
        try {
            int iterationCount = 1000;//Integer.parseInt(request.getParameter("iterationCount"));
            int keySize = 128;//Integer.parseInt(request.getParameter("keySize"));
            AesUtil aesUtil = new AesUtil(keySize, iterationCount);
            String outPut = aesUtil.encrypt(salt, iv, passphrase, plaintext);
            cphertext = Base64.decode(outPut);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cphertext;

    }
    
    
    

    private AxiomRSAKeyPair generateAxiomRSAKeyPairInner(int iLength) throws NoSuchAlgorithmException {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(iLength);
            KeyPair keyPair = kpg.genKeyPair();
            //byte[] pri = keyPair.getPrivate().getEncoded();
            //byte[] pub = keyPair.getPublic().getEncoded();
            AxiomRSAKeyPair arsakp = new AxiomRSAKeyPair();
            arsakp.visiblekey = keyPair.getPublic();
            arsakp.privatekey = keyPair.getPrivate();
            return arsakp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] encryptRSAInner(byte[] plainData, PublicKey pubKey) {
        try {
            byte[] encryptionByte = null;
            //Cipher cipher = Cipher.getInstance("RSA");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            //encryptionByte = cipher.doFinal(plainData);
            byte[] someData = cipher.update(plainData);
            byte[] moreData = cipher.doFinal();
            byte[] encrypted = new byte[someData.length + moreData.length];
            System.arraycopy(someData, 0, encrypted, 0, someData.length);
            System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);

            encryptionByte = encrypted;
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] encryptRSAForAndroidInner(byte[] plainData, PublicKey pubKey) {
        try {
            byte[] encryptionByte = null;
            //Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            encryptionByte = cipher.doFinal(plainData);
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    private byte[] encryptRSAForWebtrust(byte[] plaindata,PublicKey pkey)
    {    byte[] encryptionByte = null;
        try
        {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pkey);
          
            byte[] someData = cipher.update(plaindata);
            byte[] moreData = cipher.doFinal();
            byte[] encrypted = new byte[someData.length + moreData.length];
            System.arraycopy(someData, 0, encrypted, 0, someData.length);
            System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);
            encryptionByte=encrypted;
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
            
        return encryptionByte;
    }
    
    

    private byte[] decryptRSAInner(byte[] encrptdByte, PrivateKey privateKey) {
        try {
            byte[] encryptionByte = null;
            //Cipher cipher = Cipher.getInstance("RSA");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            //encryptionByte = cipher.doFinal(encrptdByte);
            //System.out.println("Recovered String     :::  " + new String(encryptionByte));
            byte[] someDecrypted = cipher.update(encrptdByte);
            byte[] moreDecrypted = cipher.doFinal();
            byte[] decrypted = new byte[someDecrypted.length + moreDecrypted.length];
            System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
            System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);

            encryptionByte = decrypted;

            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] decryptRSAForAndroidInner(byte[] encrptdByte, PrivateKey privateKey) { 

        try {
            byte[] encryptionByte = null;
            //Cipher cipher = Cipher.getInstance("RSA/NONE/PKCS1Padding");
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            encryptionByte = cipher.doFinal(encrptdByte);
            //System.out.println("Recovered String     :::  " + new String(encryptionByte));
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    private byte[] signDataRSAInner(byte[] data, PrivateKey privateKey, int type) {
        try {
            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA256WithRSA");
            }
            signature.initSign(privateKey);
            MessageDigest md = MessageDigest.getInstance("SHA256");
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);
            byte[] sig = signature.sign();
            //String b64Hash = new String(Base64.encode(hashedData));
            //System.out.println("Hash To be Signed is:" + b64Hash);
            //String b64Str = new String(Base64.encode(sig));
            //System.out.println("Signature is:" + b64Str);
            //String b64Str1 = new String(Base64.encode(data));
            //System.out.println("Data is:" + b64Str1);
            //return signature.sign();
            return sig;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    
     private byte[] signDataRSAInnerSha266(byte[] data, PrivateKey privateKey, String hashType,int devicetype) {
        try {
            Signature signature = null;
            if(devicetype==ANDROID){
            signature = Signature.getInstance(hashType+"WithRSA");
            }
            if(devicetype==IOS)
            {
             signature = Signature.getInstance("RSA");
            }
            signature.initSign(privateKey);
            MessageDigest md = MessageDigest.getInstance(hashType);
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);
            byte[] sig = signature.sign();
            return sig;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
     
    
    
    private byte[] signDataRSAInnerforWebtrust(byte[] data, PrivateKey privateKey, int type) {
        try {
            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA1WithRSA");
            }
            signature.initSign(privateKey);
            signature.update(data);
            byte[] sig = signature.sign();
             return sig;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private boolean verifyDataRSAInner(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        try {

            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA1WithRSA");
            }
            signature.initVerify(publicKey);

            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);

            return signature.verify(sigBytes);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    private boolean verifyDataRSAInnerv2(byte[] data, byte[] sigBytes, PublicKey publicKey, int type,String hashalgo) {
        try {

            Signature signature = null;
            if (type == IOS) {
                signature = Signature.getInstance("RSA", "BC");
            } 
            if(type==ANDROID) {
                signature = Signature.getInstance(hashalgo+"WithRSA");
            }
            signature.initVerify(publicKey);
            MessageDigest md = MessageDigest.getInstance(hashalgo);
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);

            return signature.verify(sigBytes);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    
     private boolean verifyDataRSAInnerSHA256(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        try {

            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA256withRSA");
            }
            signature.initVerify(publicKey);

            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);

            return signature.verify(sigBytes);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    
    private boolean verifyDataRSAInnerWebTrust(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        try {
            
            Security.addProvider(new BouncyCastleProvider());
            Signature sig = Signature.getInstance("SHA1WithRSA", "BC");
            sig.initVerify(publicKey);

            //System.out.println("Data" + data.length);
            sig.update(data);
          
            //System.out.println("Sign" + sigBytes.length);
            boolean b = sig.verify(sigBytes);
            
            

          
            return b;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    
    
    private boolean verifyDataRSAInner1(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        try {

            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA1WithRSA");
            }
            signature.initVerify(publicKey);

//            MessageDigest md = MessageDigest.getInstance("SHA1");
//            md.update(data);
//            byte[] hashedData = md.digest();
            signature.update(data);

            return signature.verify(sigBytes);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public PrivateKey getPrivateKeyfromPemString(String key1) {
        PrivateKey pk = null;
        String key = "MIICXAIBAAKBgQCNabJlQia/xLHAeonPAEa0lqi+0s0x9axnZo+4MkY0UMd6GvdBFjOtmyeML2fIJNvGCZnWUq1BItHiq07GEmbYyVUHBJ60IHWQNrvnwi2IJWhvMhzfT/Q+VB0aDAXomOiDqcOAdt+kkyc9p78de6//Fb+Y2DymROfK3NDu9UKKqwIDAQABAoGARDer7nsV9EavtPmoByrL8QwIxV0tzZ8FFzB7vlDKWyxovjfJ9ST/swz47ABmYT+eoY86HhDuMVDZzS30p0pzP/4kGkXxTvajBlqo5RLUIe6q3bsIJsWiVL2UBoy+Dxk1Erw3BytQvLK+Zn3MKHHBKKP4Kw86TZYIeea/J47dAkECQQDx5qcZaB+L90FO7qJ5V5Crf+S7SKodaXJFrLLyNYcJknAp/3PEi4aYv1dHyP5AFUq2rfSglIx9pXMhF86SWsuLAkEAlaevVHFNvHRKsobhxPnIle1U0dABv4aK/HsbQyNnvoKrTJNp0oAaKQ3nO0huKs4tO4GvbducItMFFtW7c6uhYQJALU7JT33sIrgd+F3D+rxG8YhT3CxBCJ8+cwRkw+74qvdNoOAi29ZSUHvOKjjgPjp+svakEgugwMvjY3hlDY6J/wJAZIQWHQ9k+Pja2wIBTf2/HgQj/jBJnExPubkt3HBBVBIL4Aj7AKmN6Jkv//9sD1AfzWITZCQmwLUfK/EuUEiswQJBAOCYBWJFWVEBUEMpaNSwSNreXdDelQNI/uWAzq7zokdENsWHePzOMRnhLhNCNaQ2HZbphG0I/NgAS8N1aQhK1sg=";
        String privKeyPEM = key.replace(
                "-----BEGIN RSA PRIVATE KEY-----\n", "")
                .replace("-----END RSA PRIVATE KEY-----", "");

// Base64 decode the data
        byte[] encodedPrivateKey = Base64.decode(privKeyPEM);

        try {
            ASN1Sequence primitive = (ASN1Sequence) ASN1Sequence
                    .fromByteArray(encodedPrivateKey);
            Enumeration<?> e = primitive.getObjects();
            BigInteger v = ((DERInteger) e.nextElement()).getValue();

            int version = v.intValue();
            if (version != 0 && version != 1) {
                throw new IllegalArgumentException("wrong version for RSA private key");
            }
            /**
             * In fact only modulus and private exponent are in use.
             */
            BigInteger modulus = ((DERInteger) e.nextElement()).getValue();
            BigInteger publicExponent = ((DERInteger) e.nextElement()).getValue();
            BigInteger privateExponent = ((DERInteger) e.nextElement()).getValue();
            BigInteger prime1 = ((DERInteger) e.nextElement()).getValue();
            BigInteger prime2 = ((DERInteger) e.nextElement()).getValue();
            BigInteger exponent1 = ((DERInteger) e.nextElement()).getValue();
            BigInteger exponent2 = ((DERInteger) e.nextElement()).getValue();
            BigInteger coefficient = ((DERInteger) e.nextElement()).getValue();

            RSAPrivateKeySpec spec = new RSAPrivateKeySpec(modulus, privateExponent);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            pk = kf.generatePrivate(spec);

        } catch (IOException e2) {
            throw new IllegalStateException();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        } catch (InvalidKeySpecException e) {
            throw new IllegalStateException(e);
        }
        return pk;

    }

    private byte[] generateKeyAESInner(byte[] keyStart, int iLength) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(keyStart);
            // 128, 192 and 256 bits may not be available
            kgen.init(iLength, sr);
            SecretKey skey = kgen.generateKey();
            byte[] key = skey.getEncoded();
            return key;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    private byte[] generateKeyAESForAndroidInner(char[] password, int iLength) {
        try {
            int iterationCount = 1000;
            int keyLength = iLength;

            KeySpec keySpec = new PBEKeySpec(password, iv,
                    iterationCount, keyLength);
            SecretKeyFactory keyFactory = SecretKeyFactory
                    .getInstance("PBKDF2WithHmacSHA1");
            byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
            SecretKey skey = new SecretKeySpec(keyBytes, "AES");
            byte[] key = skey.getEncoded();

           // System.out.println("The AES encryption key is " + new String(Base64.encode(key)));

//            byte[] encrypted = null;
//            try {
//                SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
//                Cipher cipher = Cipher.getInstance(AES_ALGO);
//                IvParameterSpec ivspec = new IvParameterSpec(iv);
//                cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
//                String data = "test data to be encrypt and decrypt";
//                byte[] someData = cipher.update(data.getBytes());
//                byte[] moreData = cipher.doFinal();
//                encrypted = new byte[someData.length + moreData.length];
//                System.arraycopy(someData, 0, encrypted, 0, someData.length);
//                System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);
//                //return encrypted;
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                return null;
//            }
//            
//            byte[] decrypted =null;
//
//            try {
//                SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
//                Cipher cipher = Cipher.getInstance(AES_ALGO);
//                IvParameterSpec ivspec = new IvParameterSpec(iv);
//                cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
//                byte[] someDecrypted = cipher.update(encrypted);
//                byte[] moreDecrypted = cipher.doFinal();
//                decrypted = new byte[someDecrypted.length + moreDecrypted.length];
//                System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
//                System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);                
//                
//                String dataDecrypted = new String(decrypted);
//                System.out.println(dataDecrypted);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                return null;
//            }
//            
//            KeyGenerator kgen = KeyGenerator.getInstance("AES");
//            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
//            sr.setSeed(keyStart);
//            // 128, 192 and 256 bits may not be available
//            kgen.init(iLength, sr);
//            SecretKey skey = kgen.generateKey();
//            byte[] key = skey.getEncoded();
            return key;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    private byte[] encryptAESInner(byte[] key, byte[] clear) {
        try {
            //String key1 = ")!@(#*$&%^123098";
            //key = key1.getBytes();

            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance(AES_ALGO);
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
            byte[] encrypted = cipher.doFinal(clear);

//            byte[] someData = cipher.update(clear);
//            byte[] moreData = cipher.doFinal();
//                        
//            byte[] encrypted = new byte[someData.length + moreData.length];
//            System.arraycopy(someData, 0, encrypted, 0, someData.length);
//            System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);
            //System.out.println("Encrypted Data::" + new String(Base64.encode(encrypted)));
            return encrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] decryptAESInner(byte[] key, byte[] encrypted) {
        try {
            //String key1 = ")!@(#*$&%^123098";
            //key = key1.getBytes();

            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance(AES_ALGO);
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
//cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] decrypted = cipher.doFinal(encrypted);
//            byte[] someDecrypted = cipher.update(encrypted);
//            byte[] moreDecrypted = cipher.doFinal();
//            byte[] decrypted = new byte[someDecrypted.length + moreDecrypted.length];
//            System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
//            System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);
            return decrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String ConsumeMobileTrust(String jsonData, String reciverPrivateKey, boolean bVerifyNeeded, int timeofActivation,String hashAlgo) {
        return ConsumeMobileTrustInner(jsonData, reciverPrivateKey, bVerifyNeeded,timeofActivation,hashAlgo);
    }

    private String ConsumeMobileTrustInner(String jsonData, String reciverPrivateKey, boolean bVerifyNeeded, int timeofActivation,String hashAlgo) {
        try {
            CryptoManager cm = new CryptoManager();
            JSONObject jsonObj = new JSONObject(jsonData);
            String _data = jsonObj.getString("_data");
            String _key = jsonObj.getString("_key");
            String _signature = jsonObj.getString("_signature");
            String senderPublicKey = jsonObj.getString("_visiblekey");
            String device = jsonObj.getString("_deviceType");
            int devicetype = 1;
            if (device.equals("ANDROID")) {
                devicetype = ANDROID;
            } else if (device.equals("IOS")) {
                devicetype = IOS;
            }
            if (device.equals("WebBrowser")) {
                devicetype = webtrust;

            }
            byte[] encdataBytes = Base64.decode(_data);
            byte[] encKeyBytes = Base64.decode(_key);
            byte[] singatureBytes = Base64.decode(_signature);
            byte[] signedDataForWebTrust=null;
            if(devicetype==webtrust){
            String singatureBytes1 = new String(singatureBytes);
            String singatureBytes1p = singatureBytes1.replaceAll("\\s+","");
              signedDataForWebTrust=CryptoManager.hexStringToByteArray(singatureBytes1p);
            }
          
            // byte[] reciverPrivateKeyBytes = Base64.decode(reciverPrivateKey);
//         String privateKeyb64 = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIGgEcisv3X/0YWIG7fPGK5qGd9HJrM3oARqNzT+r0uKyobqg2hXmNfhc+SOd77CImm2GY0TeADMqncMTGCc82VM6gXqr4AM0Jq0Ew4esqiAGbdeu0BspSth5gcXz6Rez1h0471wTCFcvtE/u7Tgd0fqKbUdwWE0cfiAxkbsfQOJAgMBAAECgYBAJihG6H7D9bsJ20bkWx/6yNVO8xZK4P9Wz/3MQzIbL290/z/S9m7Uf2VsObk29To0mILilzzvIFpIGhT+Rw4IYn4zyXe2Iri9OC5fJEqptjYYJ9a0FsO9DVhODWi8SUV9CMVoZ69M1OIl9hVUrkPRwkgqyawHeCIGLYuKhhrlVQJBALs459Gkk6YQOGFIYpFOc0Yv8iy79RYw14TapZmLaVGqioj1FzEbmFE8AL9kG9l9hKqQCYYI3LpyEhyiV1fRqe8CQQCxPoWeo5K1I8qb4soH1QyLsjMFkUuYMLdHECMlqsYI4BV0oYgQAaNFyV96EpruxWoyVRf5s1CT2I7NxPLX6oIHAkALFkacIpvfxKwiDrBPnI61BFfaEFNmOgQ5SN1vp1LYVDoZ/DGgZdryTJRawSnpCkbV9uupdVLk86Zg7bgwaikfAkAqIWxtiwAiYoGUkFHpjrDOu+r41dKcOGg4UhOornEoRuuSr5rCA0GmIvm48Jc3TmGx2Rw71G3A1ucK7lezDyEdAkEAlMGBLmJolvWf+4572MJ6+ozLYtRuI7YvxE2AtksE5LiMRLXSuzEBgW8NZwwkmo/cBfDD+JdWw8xWydNJdbXfAQ==";
//         byte[] bEncrytpedData = Base64.decode("dbjDPfHYL4rSdpDkZWVTxI6zYvSnHxH/i1h5R2jpJtpxZigUFVlda4PR1s gBEd6wLtxWLWSGCgsH8CFVOCxUx/qqpb DyIFUhdLy6cw2wfIKTCDUvwtXdATCqaNsfBYxUcnEVzTpTGO wIcwnmhjA6XtIa1G233HMThy8Lt4G0=");
         
      //   String serverPrivateKey="MIICXAIBAAKBgQCNabJlQia/xLHAeonPAEa0lqi+0s0x9axnZo+4MkY0UMd6GvdBFjOtmyeML2fIJNvGCZnWUq1BItHiq07GEmbYyVUHBJ60IHWQNrvnwi2IJWhvMhzfT/Q+VB0aDAXomOiDqcOAdt+kkyc9p78de6//Fb+Y2DymROfK3NDu9UKKqwIDAQABAoGARDer7nsV9EavtPmoByrL8QwIxV0tzZ8FFzB7vlDKWyxovjfJ9ST/swz47ABmYT+eoY86HhDuMVDZzS30p0pzP/4kGkXxTvajBlqo5RLUIe6q3bsIJsWiVL2UBoy+Dxk1Erw3BytQvLK+Zn3MKHHBKKP4Kw86TZYIeea/J47dAkECQQDx5qcZaB+L90FO7qJ5V5Crf+S7SKodaXJFrLLyNYcJknAp/3PEi4aYv1dHyP5AFUq2rfSglIx9pXMhF86SWsuLAkEAlaevVHFNvHRKsobhxPnIle1U0dABv4aK/HsbQyNnvoKrTJNp0oAaKQ3nO0huKs4tO4GvbducItMFFtW7c6uhYQJALU7JT33sIrgd+F3D+rxG8YhT3CxBCJ8+cwRkw+74qvdNoOAi29ZSUHvOKjjgPjp+svakEgugwMvjY3hlDY6J/wJAZIQWHQ9k+Pja2wIBTf2/HgQj/jBJnExPubkt3HBBVBIL4Aj7AKmN6Jkv//9sD1AfzWITZCQmwLUfK/EuUEiswQJBAOCYBWJFWVEBUEMpaNSwSNreXdDelQNI/uWAzq7zokdENsWHePzOMRnhLhNCNaQ2HZbphG0I/NgAS8N1aQhK1sg=";
        // String serverCertPemString="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCNabJlQia/xLHAeonPAEa0lqi+0s0x9axnZo+4MkY0UMd6GvdBFjOtmyeML2fIJNvGCZnWUq1BItHiq07GEmbYyVUHBJ60IHWQNrvnwi2IJWhvMhzfT/Q+VB0aDAXomOiDqcOAdt+kkyc9p78de6//Fb+Y2DymROfK3NDu9UKKqwIDAQAB"; 
           byte[] reciverPrivateKeyBytes = Base64.decode(reciverPrivateKey);
            PrivateKey pk= null;
           
            
            //decerypt the channel private key
            byte[] privkey = AxiomProtect.AccessDataBytes(reciverPrivateKeyBytes);
           
            KeyFactory kf = KeyFactory.getInstance("RSA");
            //byte[] privkey = Base64.decode(pk);
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
             pk = kf.generatePrivate(ks);
           // System.out.println("Private Key String"+new String(Base64.encode(pk.getEncoded())));
            //byte[] aeskey = cm.decryptRSA(encKeyBytes, pk);
            //System.out.println(Base64.encode(aeskey));
            //new addition for handlng NONE and ECB problem in android
            byte[] aeskey = null;
            if (devicetype == ANDROID) {
                aeskey = cm.decryptRSAForAndroid(encKeyBytes, pk);
            } else if (devicetype == IOS) {
                aeskey = cm.decryptRSA(encKeyBytes, pk);
            }
            else if(devicetype==webtrust)
            {
               aeskey = cm.decryptRSAForAndroid(encKeyBytes, pk);
                
            }
            
              byte[] plaindataBytes=null;
             if(device.equals("WebBrowser"))
            {
            String textAesKey=new String(aeskey);
              
        plaindataBytes= cm.decryptAesforWebTrust(textAesKey,textAesKey,textAesKey, _data);
             }
             else
             {
           
          plaindataBytes = cm.decryptAES(aeskey, encdataBytes);
            //end of addition
             } 
            
            if (bVerifyNeeded == true) {
                //KeyFactory kf = KeyFactory.getInstance("RSA");

                if (devicetype == ANDROID) {
                    X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(senderPublicKey));
                    KeyFactory fact = KeyFactory.getInstance("RSA");
                    PublicKey publicKey = fact.generatePublic(spec);
                  //  bVerifyNeeded = cm.verifyDataRSA(plaindataBytes, singatureBytes, publicKey, SHA1WithRSA);
                    bVerifyNeeded = cm.verifyDataRSAv2(plaindataBytes, singatureBytes, publicKey,ANDROID,hashAlgo);
                  
                } else if (devicetype == IOS) {
                        PublicKey publicKey = null;
                    if(timeofActivation == ACTIVATE){
                          publicKey = this.getPublicKey(senderPublicKey);
                    }else{
                  
                    X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(senderPublicKey));
                    KeyFactory fact = KeyFactory.getInstance("RSA");
                     publicKey = fact.generatePublic(spec);
                    }
                   // bVerifyNeeded = cm.verifyDataRSA(plaindataBytes, singatureBytes, publicKey, RSA);
                      bVerifyNeeded =   true ; //cm.verifyDataRSAv2(plaindataBytes, singatureBytes, publicKey,IOS,hashAlgo);
                    
                }
                if(devicetype==webtrust)
                {
                 
                    byte[] dataq =Base64.encode(plaindataBytes);
                    String datap=new String(dataq);
                    String datar=datap.replaceAll("[\\s\\u00A0]+$", "");
                    
                 String publicKeyPEM = senderPublicKey.replace("-----BEGIN PUBLIC KEY-----", "");
                   publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");
   

                    
                    
//            X509EncodedKeySpec bobPubKeySpec = new X509EncodedKeySpec(Base64.decode(serverCertPemString));
//            Security.addProvider(new BouncyCastleProvider());
//            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//            PublicKey publicKey = keyFactory.generatePublic(bobPubKeySpec);
                     PublicKey publicKey = null;
                     X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(publicKeyPEM));
                      KeyFactory fact = KeyFactory.getInstance("RSA");
                     publicKey = fact.generatePublic(spec);
                     
            bVerifyNeeded = cm.verifyDataRSAInnerWebTrust(datar.getBytes(),signedDataForWebTrust, publicKey,RSA);
                    
                  
                }
                
                
            } else {
                return new String(plaindataBytes);
            }

            if (bVerifyNeeded == true) {
                return new String(plaindataBytes);
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;

        //return jsonData;
    }

        public String EnforceMobileTrust(JSONObject json, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType, String userid, String channelid,String hashAlgo) {
        return EnforceMobileTrustInner(json, password, recieverPublicKey, senderPrivateKey, senderPublicKey, deviceType,userid,channelid,hashAlgo);
    }
    
     public String EnforceMobileTrustForUpdate(JSONObject json, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType,String pfxPassword,String pin,String hashAlgo) {
        return EnforceMobileTrustInnerForUpdate(json, password, recieverPublicKey, senderPrivateKey, senderPublicKey, deviceType,pfxPassword,pin,hashAlgo);
    }
//    public byte[] AesDecryptionWebTrust(String passphrase,String salt,String iterationCount,int keySize)
//    {
//        try
//        {
//         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//         SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
//    KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), as(salt), iterationCount, keySize);
//    SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
//        }
//        catch(Exception ex)
//        {
//            ex.printStackTrace();
//        }
//        
//    }
    
    

    private String EnforceMobileTrustInner(JSONObject json, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType, String userid, String channelid,String hashAlgo) {
        try {
            CryptoManager cm = new CryptoManager();
             if(senderPrivateKey.contains("PRIVATE"))
          {
                senderPrivateKey = senderPrivateKey.replace(
                "-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "");
          }
          
             if(recieverPublicKey.contains("PUBLIC"))
            {
                   recieverPublicKey = recieverPublicKey.replace("-----BEGIN PUBLIC KEY-----", "");
                   recieverPublicKey = recieverPublicKey.replace("-----END PUBLIC KEY-----", "");               
            }
          
            byte[] privkey = AxiomProtect.AccessDataBytes(Base64.decode(senderPrivateKey));

            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
            
            
           // System.out.println("Private Key for Enforce"+new String(Base64.encode(pk.getEncoded())));
            //new json data needs to be created 
            byte[] signatureBytes = null;
            if (deviceType == IOS) {
                signatureBytes = cm.signDataRSAv2(json.toString().getBytes(),pk,hashAlgo, IOS);
                
            } else if (deviceType == ANDROID) {
              //  signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
                  signatureBytes = cm.signDataRSAv2(json.toString().getBytes(),pk,hashAlgo, ANDROID);
            }
            else if(deviceType==webtrust)
            {
               signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
             
            }
            //generate aes key from password
            byte[] aesKey = null;
            if (deviceType == IOS) {
                aesKey = cm.generateKeyAES(password.getBytes(), 128);
            } else if (deviceType == ANDROID) {
                aesKey = cm.generateKeyAESForAndroid(password.toCharArray(), 128);
            }
            else if(deviceType==webtrust)
            {
                    aesKey = cm.generateKeyAESForAndroid(password.toCharArray(), 128);
            }
              byte[] encryptedAESJSONDATA=null;
            //new json data needs to be encrypted with aes generated key 
            String aesKeyforEnrypt="";
              if(deviceType==webtrust)
            {
           aesKeyforEnrypt=convertToHex(aesKey);
          encryptedAESJSONDATA = cm.encryptAesforWebTrust(aesKeyforEnrypt.substring(0,32),aesKeyforEnrypt.substring(0,32),aesKeyforEnrypt.substring(0,32),json.toString());      
            }
            else
            {
           encryptedAESJSONDATA = cm.encryptAES(aesKey, json.toString().getBytes());    
                
            }
          
            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(recieverPublicKey));
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey publicKey = fact.generatePublic(spec);
            //final json data needs to be created by receiverPublic Key            
            //byte[] encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            byte[] encryptedAESKEY = null;
            if (deviceType == IOS) {
                encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            } else if (deviceType == ANDROID) {
                encryptedAESKEY = cm.encryptRSAForAndroid(aesKey, publicKey);
            }
            else if(deviceType==webtrust)
            {
              encryptedAESKEY = cm.encryptRSAForWebtrust(aesKeyforEnrypt.getBytes(), publicKey);
                
            }
              String singature ="";
             
           if(deviceType==webtrust)
           {
             singature =convertToHex(signatureBytes);
               
           }
           else
           {
              singature = new String(Base64.encode(signatureBytes));
           }
           String senderPublicKey1="";
           if(deviceType==webtrust)
           {
             senderPublicKey1="-----BEGIN PUBLIC KEY-----"+senderPublicKey+"-----END PUBLIC KEY-----";
             senderPublicKey=senderPublicKey1;
           }
            
            String data = new String(Base64.encode(encryptedAESJSONDATA));
            String key = new String(Base64.encode(encryptedAESKEY));
            Properties p = new Properties();
            p.setProperty("_signature", singature);
            p.setProperty("_data", data);
            p.setProperty("_key", key);
            p.setProperty("_visibleKey", senderPublicKey);
            p.setProperty("_userid", userid);
            p.setProperty("_channelid", channelid);
            UtilityImpl utils = new UtilityImpl();
            JSONObject jsonObjToProtect = utils.GenerateJSON(p);

            //            Base64.decode(data)
            //            plaindata = cm.decryptAES(aesKey, encryptedAESKEY)
            return jsonObjToProtect.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;

        //return json.toString();
    }
    
     private String EnforceMobileTrustInnerForUpdate(JSONObject json, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType,String pfxPassword,String pin,String hashalgo) {
        try {
            CryptoManager cm = new CryptoManager();

            //json data needs to be signed.
            //byte[] privkey = ;
            byte[] privkey = AxiomProtect.AccessDataBytes(Base64.decode(senderPrivateKey));

            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
            //new json data needs to be created 
            byte[] signatureBytes = null;
            if (deviceType == IOS) {
              //  signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, RSA);
               signatureBytes = cm.signDataRSAv2(json.toString().getBytes(), pk, hashalgo,IOS);
              
            } else if (deviceType == ANDROID) {
             //   signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
                 signatureBytes = cm.signDataRSAv2(json.toString().getBytes(), pk, hashalgo,ANDROID);
            }
            else if(deviceType==webtrust)
            {
               signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
             
            }
            //generate aes key from password
            byte[] aesKey = null;
            if (deviceType == IOS) {
                aesKey = cm.generateKeyAES(password.getBytes(), 128);
            } else if (deviceType == ANDROID) {
                aesKey = cm.generateKeyAESForAndroid(password.toCharArray(), 128);
            }
            else if(deviceType==webtrust)
            {
                    aesKey = cm.generateKeyAESForAndroid(password.toCharArray(), 128);
            }
              byte[] encryptedAESJSONDATA=null;
            //new json data needs to be encrypted with aes generated key 
            String aesKeyforEnrypt="";
              if(deviceType==webtrust)
            {
              aesKeyforEnrypt=convertToHex(aesKey);
          encryptedAESJSONDATA = cm.encryptAesforWebTrust(aesKeyforEnrypt.substring(0,32),aesKeyforEnrypt.substring(0,32),aesKeyforEnrypt.substring(0,32),json.toString());      
            }
            else
            {
           encryptedAESJSONDATA = cm.encryptAES(aesKey, json.toString().getBytes());    
                
            }
          
            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(recieverPublicKey));
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey publicKey = fact.generatePublic(spec);
            //final json data needs to be created by receiverPublic Key            
            //byte[] encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            byte[] encryptedAESKEY = null;
            if (deviceType == IOS) {
                encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            } else if (deviceType == ANDROID) {
                encryptedAESKEY = cm.encryptRSAForAndroid(aesKey, publicKey);
            }
            else if(deviceType==webtrust)
            {
              encryptedAESKEY = cm.encryptRSAForWebtrust(aesKeyforEnrypt.getBytes(), publicKey);
                
            }
              String singature ="";
             
           if(deviceType==webtrust)
           {
             singature =convertToHex(signatureBytes);
               
           }
           else
           {
              singature = new String(Base64.encode(signatureBytes));
           }
            
            
            String data = new String(Base64.encode(encryptedAESJSONDATA));
            String key = new String(Base64.encode(encryptedAESKEY));

            CryptoManager cManager = new CryptoManager();
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("pfxPassword", pfxPassword);
                jsonObj.put("password", pin);
//                    byte[] bytePin = password.getBytes();
//                    bytePin = cManager.generateKeyAES(bytePin, 128);
//                    byte[] byteJson = json.toString().getBytes();
//                    byte[] byteResult = cManager.encryptAES(bytePin, byteJson);

           String base64Result = new String(Base64.encode(jsonObj.toString().getBytes()));
            
            Properties p = new Properties();
            p.setProperty("_signature", singature);
            p.setProperty("_data", data);
            p.setProperty("_key", key);
            p.setProperty("_visibleKey", senderPublicKey);
            p.setProperty("_secretKey", base64Result);
            UtilityImpl utils = new UtilityImpl();
            JSONObject jsonObjToProtect = utils.GenerateJSON(p);

            //            Base64.decode(data)
            //            plaindata = cm.decryptAES(aesKey, encryptedAESKEY)
            return jsonObjToProtect.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;

        //return json.toString();
    }
    
    
      private String EnforceMobileTrustInnerbyUserId(JSONObject json, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType) {
        try {
            CryptoManager cm = new CryptoManager();

            //json data needs to be signed.
            //byte[] privkey = ;
            byte[] privkey = AxiomProtect.AccessDataBytes(Base64.decode(senderPrivateKey));

            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
            //new json data needs to be created 
            byte[] signatureBytes = null;
            if (deviceType == IOS) {
                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, RSA);
            } else if (deviceType == ANDROID) {
                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
            }
            //generate aes key from password
            byte[] aesKey = null;
            if (deviceType == IOS) {
                aesKey = cm.generateKeyAES(password.getBytes(), 128);
            } else if (deviceType == ANDROID) {
                aesKey = cm.generateKeyAESForAndroid(password.toCharArray(), 128);
            }
            //new json data needs to be encrypted with aes generated key 
            byte[] encryptedAESJSONDATA = cm.encryptAES(aesKey, json.toString().getBytes());
            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(recieverPublicKey));
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey publicKey = fact.generatePublic(spec);
            //final json data needs to be created by receiverPublic Key            
            //byte[] encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            byte[] encryptedAESKEY = null;
            if (deviceType == IOS) {
                encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            } else if (deviceType == ANDROID) {
                encryptedAESKEY = cm.encryptRSAForAndroid(aesKey, publicKey);
            }

            String singature = new String(Base64.encode(signatureBytes));
            String data = new String(Base64.encode(encryptedAESJSONDATA));
            String key = new String(Base64.encode(encryptedAESKEY));

            Properties p = new Properties();
            p.setProperty("_signature", singature);
            p.setProperty("_data", data);
            p.setProperty("_key", key);
            p.setProperty("_visibleKey", senderPublicKey);
            UtilityImpl utils = new UtilityImpl();
            JSONObject jsonObjToProtect = utils.GenerateJSON(p);

            //            Base64.decode(data)
            //            plaindata = cm.decryptAES(aesKey, encryptedAESKEY)
            return jsonObjToProtect.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;

        //return json.toString();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public String MobileTrustSignOnly(JSONObject json, String senderPrivateKey, int devicetype,String hashAlgo) {
        return MobileTrustSignOnlyInner(json, senderPrivateKey, devicetype,hashAlgo);
    }
    
    public String MobileTrustSignOnlyForWebTrust(JSONObject json, String senderPrivateKey, int devicetype) {
           CryptoManager cm = new CryptoManager();
           try{
            byte[] privkey = AxiomProtect.AccessDataBytes(Base64.decode(senderPrivateKey));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
         //   System.out.println("PrivateKey"+new String(Base64.encode(pk.getEncoded())));
            byte[] signatureBytes = null;
            if (devicetype == ANDROID) {
                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
            } else if (devicetype == IOS) {
                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, RSA);
                
            }
            else if(devicetype==webtrust)
            {
             signatureBytes = cm.signDataRSAforWebtrust(json.toString().getBytes(), pk, SHA1WithRSA);
            }
            String singature = convertToHex(signatureBytes);

            String data = new String(Base64.encode(json.toString().getBytes()));

            Properties p = new Properties();

            p.setProperty("_signature", singature);
            p.setProperty("_data", data);
            UtilityImpl utils = new UtilityImpl();
            JSONObject jsonObjToProtect = utils.GenerateJSON(p);
            utils = null;
            return jsonObjToProtect.toString();
           }catch(Exception ex)
           {
           ex.printStackTrace();
           }
       
        return null;
    }
    

    private String MobileTrustSignOnlyInner(JSONObject json, String senderPrivateKey, int devicetype,String hashAlgo) {
        try {
            CryptoManager cm = new CryptoManager();
            //byte[] privkey = Base64.decode(senderPrivateKey);
            byte[] privkey = AxiomProtect.AccessDataBytes(Base64.decode(senderPrivateKey));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
            byte[] signatureBytes = null;
//            if (devicetype == ANDROID) {
//                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
//            } else if (devicetype == IOS) {
              //  signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, RSA);
                 signatureBytes = cm.signDataRSAv2(json.toString().getBytes(), pk,hashAlgo,devicetype);
           // }
            String singature = new String(Base64.encode(signatureBytes));

            String data = new String(Base64.encode(json.toString().getBytes()));

            Properties p = new Properties();

            p.setProperty("_signature", singature);
            p.setProperty("_data", data);
            UtilityImpl utils = new UtilityImpl();
            JSONObject jsonObjToProtect = utils.GenerateJSON(p);
            utils = null;
            return jsonObjToProtect.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;
    }

//    public boolean CheckAESEncrypt() {
//        try {
//            CryptoManager cmObj = new CryptoManager();
//            byte[] aeskey = cmObj.generateKeyAES("vikram sareen is my name".getBytes(), 128);
//            String strData = "data to sign 1.data to sign 2.data to sign 3.data to sign 4.data to sign 5.data to sign 6.data to sign 7.data to sign 8.data to sign 9.data to sign 10.";
//            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData.getBytes());
//            System.out.println("Main Activity Encrypted AES Data:" + new String(Base64.encode(byteEncryptedData)));
//
//            CryptoManager cmObjDecrypt = new CryptoManager();
//            byte[] byteDecryptData = cmObjDecrypt.decryptAES(aeskey, byteEncryptedData);
//            System.out.println("Main Activity Decrypted AES Data:" + new String(byteDecryptData));
//
//            return true;
//
//        } catch (Exception e) {
//            return false;
//        }
//    }
//    private PublicKey getPublicKey(String publicKey) {
//        try {
//
////            byte[] keyBytes = Base64.decode(publicKey);;
////            ASN1InputStream in = new ASN1InputStream(keyBytes);
////            DERObject obj = in.readObject();
////            RSAPublicKeyStructure pStruct = RSAPublicKeyStructure.getInstance(obj);
////            RSAPublicKeySpec spec = new RSAPublicKeySpec(pStruct.getModulus(), pStruct.getPublicExponent());
////            KeyFactory kf = KeyFactory.getInstance("RSA");
////            PublicKey pubk = kf.generatePublic(spec);
//            String pemKey = "-----BEGIN RSA PUBLIC KEY-----\n"
//                    + publicKey + "\n"
//                    + "-----END RSA PUBLIC KEY-----\n";
//         InputStream    pemStream = new ByteArrayInputStream(pemKey.getBytes("UTF-8"));
//            Reader pemReader = new BufferedReader(new InputStreamReader(pemStream));
//        PEMParser pemParser = new PEMParser(pemReader);
//            Object parsedObj=null;
//        try {
//             parsedObj = pemParser.readObject();
//        }catch(Exception ex)
//            
//        {
//            ex.printStackTrace();
//        }
//            RSAPublicKey rsaPubKey = (RSAPublicKey)parsedObj;
////            System.out.println("Public key: " + rsaPubKey);
//            PublicKey pubk = rsaPubKey;
//            //   byte[] s = Base64.encode(pubk.getEncoded());
////        String vKey = new String(s);
////        System.out.println("Public Key   ::  " + vKey);
//            return pubk;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
    
    
    //method Defined by Manoj to solve problem of iossdk issue 7-8-2015
    
       private PublicKey getPublicKey(String publicKey) {
        try {

//            byte[] keyBytes = Base64.decode(publicKey);;
//            ASN1InputStream in = new ASN1InputStream(keyBytes);
//            DERObject obj = in.readObject();
//            RSAPublicKeyStructure pStruct = RSAPublicKeyStructure.getInstance(obj);
//            RSAPublicKeySpec spec = new RSAPublicKeySpec(pStruct.getModulus(), pStruct.getPublicExponent());
//            KeyFactory kf = KeyFactory.getInstance("RSA");
//            PublicKey pubk = kf.generatePublic(spec);
            String pemKey = "-----BEGIN RSA PUBLIC KEY-----\n"
                    + publicKey + "\n"
                    + "-----END RSA PUBLIC KEY-----\n";
            InputStream    pemStream = new ByteArrayInputStream(pemKey.getBytes("UTF-8"));
            Reader pemReader = new BufferedReader(new InputStreamReader(pemStream));
        PEMParser pemParser = new PEMParser(pemReader);
Object parsedObj=null;
        try {
             parsedObj = pemParser.readObject();
        }catch(Exception ex)
            
        {
            ex.printStackTrace();
        }
        
         // kp = (PEMKeyPair) pemParser.readObject();
            SubjectPublicKeyInfo info = (SubjectPublicKeyInfo) parsedObj;
       RSAPublicKey rsaPubKey = (RSAPublicKey) new JcaPEMKeyConverter().setProvider("BC").getPublicKey(info);
        //   RSAPublicKey rsaPubKey = (RSAPublicKey)parsedObj;
//            System.out.println("Public key: " + rsaPubKey);
            PublicKey pubk = rsaPubKey;
            //   byte[] s = Base64.encode(pubk.getEncoded());
//        String vKey = new String(s);
//        System.out.println("Public Key   ::  " + vKey);
            return pubk;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //newly added
    public String decryptSecreteAES128(byte[] secrete, String filename) throws NoSuchAlgorithmException {

        File f = new File(filename);
        try {
            byte[] key = this.read(f);
            //System.out.println("password key length::" + key.length);
            //System.out.println("plain secrete length::" + secrete.length);
            byte[] secrte = decryptAES(key, secrete);
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(secrte);
            byte[] output = md.digest();
            //System.out.println("decrypted secrete length::" + secrte.length);
            //byte[] sec = Base64.decode("MDkMYHMLyFkWzP20t+tcI5mSyfk=");
            //System.out.println("plain secret:: MDkMYHMLyFkWzP20t+tcI5mSyfk=");
            byte[] strPS = Base64.decode("MDkMYHMLyFkWzP20t+tcI5mSyfk=");
            //System.out.println("decrypted secret::" + new String(Base64.encode(secrte)));
            //System.out.println(new String(secrte));
            //  byte[] s = Base64.decode(secrte);

            // return this.convertToHex(s);
            //System.out.println(new String( this.convertToHex(sec)));
            //System.out.println(new String( this.convertToHex(secrte)));
        } catch (IOException ex) {
            Logger.getLogger(CryptoManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public String decryptSecreteAES256(byte[] secrete, String filename) {

        File f = new File(filename);
        try {
            byte[] key = this.read(f);
            byte[] secrte = decryptAES(key, secrete);
            byte[] sec = Base64.decode("MDkMYHMLyFkWzP20t+tcI5mSyfk=");
            //System.out.println(new String(sec));
            //System.out.println(new String(secrte));
            //  byte[] s = Base64.decode(secrte);

            // return this.convertToHex(s);
            //System.out.println(new String(this.convertToHex(sec)));
            //System.out.println(new String(this.convertToHex(secrte)));
        } catch (IOException ex) {
            Logger.getLogger(CryptoManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public byte[] read(File file) throws IOException {
        byte[] buffer = new byte[(int) file.length()];
        InputStream ios = null;
        try {
            ios = new FileInputStream(file);
            if (ios.read(buffer) == -1) {
                throw new IOException("EOF reached while trying to read the whole file");
            }
        } finally {
            try {
                if (ios != null) {
                    ios.close();
                }
            } catch (IOException e) {
            }
        }

        return buffer;
    }

    //end of addition
    public byte[] DecryptAES_CBC(byte[] data, byte[] key) throws Exception {
       byte[] decryPted=null;
        try
           {
               SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
               IvParameterSpec ivspec = new IvParameterSpec(iv);
               Cipher cipher = Cipher.getInstance(AES_ALGO);
               cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
               decryPted = cipher.doFinal(data);
           }catch(Exception ex)
           {
               ex.printStackTrace();
           }
        return decryPted;
    }
    
    //adding support for AES256 CBC
    public byte[] DecryptAES256_CBC(byte[] data, byte[] key) throws Exception {
       byte[] decryPted=null;
        try
           {
               SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
               IvParameterSpec ivspec = new IvParameterSpec(iv);
               Cipher cipher = Cipher.getInstance(AES_ALGO);
               cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
               decryPted = cipher.doFinal(data);
           }catch(Exception ex)
           {
               ex.printStackTrace();
           }
        return decryPted;
    }
    

    public String ConsumeSecurity(String jsonData, String reciverPrivateKey, boolean bVerifyNeeded) {
        return ConsumeSecurityInner(jsonData, reciverPrivateKey, bVerifyNeeded);
    }

    private String ConsumeSecurityInner(String jsonData, String reciverPrivateKey, boolean bVerifyNeeded) {
        try {

            
          if(reciverPrivateKey.contains("PRIVATE"))
          {
                reciverPrivateKey = reciverPrivateKey.replace(
                "-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "");
          }
             
            JSONObject jsonObj = new JSONObject(jsonData);
            String _data = jsonObj.getString("_data");
            String _key = jsonObj.getString("_key");
            String _signature = jsonObj.getString("_signature");
            String senderPublicKey = jsonObj.getString("_visiblekey");
            String device = jsonObj.getString("_deviceType");
            int devicetype = 1;
            if (device.equals("ANDROID")) {
                devicetype = ANDROID;
            } else if (device.equals("IOS")) {
                devicetype = IOS;
            }
            CryptoManager cm = new CryptoManager();
            byte[] encdataBytes = Base64.decode(_data);
            byte[] encKeyBytes = Base64.decode(_key);
            byte[] singatureBytes = Base64.decode(_signature);

            // byte[] reciverPrivateKeyBytes = Base64.decode(reciverPrivateKey);
//         String privateKeyb64 = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIGgEcisv3X/0YWIG7fPGK5qGd9HJrM3oARqNzT+r0uKyobqg2hXmNfhc+SOd77CImm2GY0TeADMqncMTGCc82VM6gXqr4AM0Jq0Ew4esqiAGbdeu0BspSth5gcXz6Rez1h0471wTCFcvtE/u7Tgd0fqKbUdwWE0cfiAxkbsfQOJAgMBAAECgYBAJihG6H7D9bsJ20bkWx/6yNVO8xZK4P9Wz/3MQzIbL290/z/S9m7Uf2VsObk29To0mILilzzvIFpIGhT+Rw4IYn4zyXe2Iri9OC5fJEqptjYYJ9a0FsO9DVhODWi8SUV9CMVoZ69M1OIl9hVUrkPRwkgqyawHeCIGLYuKhhrlVQJBALs459Gkk6YQOGFIYpFOc0Yv8iy79RYw14TapZmLaVGqioj1FzEbmFE8AL9kG9l9hKqQCYYI3LpyEhyiV1fRqe8CQQCxPoWeo5K1I8qb4soH1QyLsjMFkUuYMLdHECMlqsYI4BV0oYgQAaNFyV96EpruxWoyVRf5s1CT2I7NxPLX6oIHAkALFkacIpvfxKwiDrBPnI61BFfaEFNmOgQ5SN1vp1LYVDoZ/DGgZdryTJRawSnpCkbV9uupdVLk86Zg7bgwaikfAkAqIWxtiwAiYoGUkFHpjrDOu+r41dKcOGg4UhOornEoRuuSr5rCA0GmIvm48Jc3TmGx2Rw71G3A1ucK7lezDyEdAkEAlMGBLmJolvWf+4572MJ6+ozLYtRuI7YvxE2AtksE5LiMRLXSuzEBgW8NZwwkmo/cBfDD+JdWw8xWydNJdbXfAQ==";
//         byte[] bEncrytpedData = Base64.decode("dbjDPfHYL4rSdpDkZWVTxI6zYvSnHxH/i1h5R2jpJtpxZigUFVlda4PR1s gBEd6wLtxWLWSGCgsH8CFVOCxUx/qqpb DyIFUhdLy6cw2wfIKTCDUvwtXdATCqaNsfBYxUcnEVzTpTGO wIcwnmhjA6XtIa1G233HMThy8Lt4G0=");
            byte[] reciverPrivateKeyBytes = Base64.decode(reciverPrivateKey);
            //decerypt the channel private key
            byte[] privkey = AxiomProtect.AccessDataBytes(reciverPrivateKeyBytes);

            KeyFactory kf = KeyFactory.getInstance("RSA");
            //byte[] privkey = Base64.decode(pk);
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);

            //byte[] aeskey = cm.decryptRSA(encKeyBytes, pk);
            //System.out.println(Base64.encode(aeskey));
            //new addition for handlng NONE and ECB problem in android
            byte[] aeskey = null;
            if (devicetype == ANDROID) {
                aeskey = cm.decryptRSAForAndroid(encKeyBytes, pk);
            } else if (devicetype == IOS) {
                aeskey = cm.decryptRSA(encKeyBytes, pk);
            }

            byte[] plaindataBytes = cm.decryptAES(aeskey, encdataBytes);
            //end of addition

            if (bVerifyNeeded == true) {
                //KeyFactory kf = KeyFactory.getInstance("RSA");

                if (devicetype == ANDROID) {
                    X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(senderPublicKey));
                    KeyFactory fact = KeyFactory.getInstance("RSA");
                    PublicKey publicKey = fact.generatePublic(spec);
                    bVerifyNeeded = cm.verifyDataRSA(plaindataBytes, singatureBytes, publicKey, SHA1WithRSA);
                } else if (devicetype == IOS) {
                    PublicKey publicKey = this.getPublicKey(senderPublicKey);
                    bVerifyNeeded = cm.verifyDataRSA(plaindataBytes, singatureBytes, publicKey, RSA);
                }
            } else {
                return new String(plaindataBytes);
            }

            if (bVerifyNeeded == true) {
                return new String(plaindataBytes);
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;

        //return jsonData;
    }

  public static byte[] hexStringToByteArray(String s) {
        byte[] data=null;
      try
      {
        int len = s.length();
        data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
      }catch(Exception ex)
      {
          ex.printStackTrace();
      }
        return data;
    }
    
    
    
    public String EnforceSecurity(String plainData, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType) {
        return EnforceSecurityInner(plainData, password, recieverPublicKey, senderPrivateKey, senderPublicKey, deviceType);
    }

    private String EnforceSecurityInner(String plainData, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType) {
        try {
            CryptoManager cm = new CryptoManager();
            if(recieverPublicKey.contains("PUBLIC"))
            {
                   recieverPublicKey = recieverPublicKey.replace("-----BEGIN PUBLIC KEY-----", "");
                   recieverPublicKey = recieverPublicKey.replace("-----END PUBLIC KEY-----", "");               
            }
          if(senderPrivateKey.contains("PRIVATE"))
          {
                senderPrivateKey = senderPrivateKey.replace(
                "-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "");
          }
            //json data needs to be signed.
            //byte[] privkey = ;
            byte[] privkey = AxiomProtect.AccessDataBytes(Base64.decode(senderPrivateKey));

            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
            //new json data needs to be created 
            byte[] signatureBytes = null;
            if (deviceType == IOS) {
                signatureBytes = cm.signDataRSA(plainData.getBytes(), pk, RSA);
            } else if (deviceType == ANDROID) {
                signatureBytes = cm.signDataRSA(plainData.getBytes(), pk, SHA1WithRSA);
            }
          
            //generate aes key from password
            byte[] aesKey = null;
            if (deviceType == IOS) {
                aesKey = cm.generateKeyAES(password.getBytes(), 128);
            } else if (deviceType == ANDROID) {
                aesKey = cm.generateKeyAESForAndroid(password.toCharArray(), 128);
            }
            //new json data needs to be encrypted with aes generated key 
           byte[] encryptedAESJSONDATA=null;
        
            if(deviceType==webtrust)
            {
                 String aesKeyforEnc=new String(aesKey);
      encryptedAESJSONDATA=cm.encryptAesforWebTrust(aesKeyforEnc, aesKeyforEnc, aesKeyforEnc,plainData);
            }
            else
            {
            encryptedAESJSONDATA = cm.encryptAES(aesKey, plainData.getBytes());
            }
            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(recieverPublicKey));
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey publicKey = fact.generatePublic(spec);
            //final json data needs to be created by receiverPublic Key            
            //byte[] encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            byte[] encryptedAESKEY = null;
            if (deviceType == IOS) {
                encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            } else if (deviceType == ANDROID) {
                encryptedAESKEY = cm.encryptRSAForAndroid(aesKey, publicKey);
            }
            String singature="";
              if(deviceType==webtrust)
              {
             singature =convertToHex(signatureBytes) ;
              }
              else
              {
               singature=new String(Base64.encode(signatureBytes));
             
              }
            String data = new String(Base64.encode(encryptedAESJSONDATA));
            String key = new String(Base64.encode(encryptedAESKEY));

            Properties p = new Properties();
            p.setProperty("_signature", singature);
            p.setProperty("_data", data);
            p.setProperty("_key", key);
            p.setProperty("_visibleKey", senderPublicKey);
            UtilityImpl utils = new UtilityImpl();
            JSONObject jsonObjToProtect = utils.GenerateJSON(p);

            //            Base64.decode(data)
            //            plaindata = cm.decryptAES(aesKey, encryptedAESKEY)
            return jsonObjToProtect.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;

        //return json.toString();
    }
    
    
     public static String  getPublikKey(PublicKey pubBytes) throws IOException
  {   SubjectPublicKeyInfo spkInfo = SubjectPublicKeyInfo.getInstance(pubBytes.getEncoded());
  ASN1Primitive primitive = spkInfo.parsePublicKey();
 byte[] publicKeyPKCS1 = primitive.getEncoded();
 PemObject pemObject = new PemObject("RSA PUBLIC KEY", publicKeyPKCS1);
StringWriter stringWriter = new StringWriter();
PemWriter pemWriter = new PemWriter(stringWriter);
pemWriter.writeObject(pemObject);
pemWriter.close();
String pemString = stringWriter.toString();
return pemString;
}
    
    
    

}
