/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.mobiletrust.crypto;

/**
 *
 * @author vikramsareen
 */
public class MobileTrustServerLicenseAttributes {
    public static final int BASE = 1;
    public static final int PLUS = 2;
    public String productverison;
    public long licenseexpiresOn;
    public int licensetype; // base or plus
    public String fetchVisibleIPURL = null;
    public String fetchGeoURL = null;
}