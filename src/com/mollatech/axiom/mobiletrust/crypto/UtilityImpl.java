package com.mollatech.axiom.mobiletrust.crypto;


import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import org.json.JSONObject;

public class UtilityImpl implements UtilityInteface {

    public final static int AES = 1;
    public final static int RSA = 2;
    public final static int AES_KEY_SIZE_192 = 192;
    
    
    public JSONObject GenerateJSON(Properties p) {
        return GenerateJSONInner(p);
    }
    
    public byte[] EncryptData(int type, Object key, byte[] data) {
        return EncryptDataInner(type, key, data);
    }

    public byte[] DecryptData(int type, Object key, byte[] encdata) {
        return DecryptDataInner(type, key, encdata);
    }

    public byte[] SignData(byte[] data, Object key) {
        return SignDataInner(data, key);
    }

    public int VerifyData(byte[] data, byte[] signature, Object key) {
        return VerifyDataInner(data, signature, key);
    }
   
    private JSONObject GenerateJSONInner(Properties p) {
        try {
            JSONObject json = new JSONObject();
            Set states = p.keySet();
            String str;
            Iterator<String> itr = states.iterator();

            while (itr.hasNext()) {
                str = (String) itr.next();
                String data = p.getProperty(str);

                json.put(str, data);
            }

            return json;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] EncryptDataInner(int type, Object key, byte[] data) {
        try {
            if (type == AES) {
                CryptoManager cm = new CryptoManager();
                String pass = (String) key;
                byte[] rawKey = cm.generateKeyAES(pass.getBytes(), AES_KEY_SIZE_192);
                byte[] encryptedDataBytes = cm.encryptAES(rawKey, data);
                cm = null;
                //return new String(Base64.encode(encryptedDataBytes));
                return encryptedDataBytes;
            } else if (type == RSA) {
                CryptoManager cm = new CryptoManager();
                PublicKey pubKey = (PublicKey) key;
                byte[] encryptedDataBytes = cm.encryptRSA(data, pubKey);
                cm = null;
                //return new String(Base64.encode(encryptedDataBytes));
                return encryptedDataBytes;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] DecryptDataInner(int type, Object key, byte[] encdata) {
        try {
            if (type == AES) {
                CryptoManager cm = new CryptoManager();
                String pass = (String) key;
                byte[] rawKey = cm.generateKeyAES(pass.getBytes(), AES_KEY_SIZE_192);
                byte[] plainDataBytes = cm.decryptAES(rawKey, encdata);
                cm = null;
                return plainDataBytes;
            } else if (type == RSA) {
                CryptoManager cm = new CryptoManager();
                PrivateKey prvKey = (PrivateKey) key;
                byte[] plainDataBytes = cm.decryptRSA(encdata, prvKey);
                cm = null;
                return plainDataBytes;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] SignDataInner(byte[] data, Object key) {
        try {
            CryptoManager cm = new CryptoManager();
            PrivateKey prvKey = (PrivateKey) key;
            byte[] signatureBytes = cm.signDataRSA(data, prvKey,RSA);//RSA(encdata, pubKey);
            cm = null;
            return signatureBytes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private int VerifyDataInner(byte[] data, byte[] signature, Object key) {
        try {
            CryptoManager cm = new CryptoManager();
            PublicKey pubKey = (PublicKey) key;
            boolean bResult = cm.verifyDataRSA(data, signature, pubKey,RSA);
            cm = null;
            if (bResult == true) {
                return 0;
            } else {
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -2;
        }
    }
}
